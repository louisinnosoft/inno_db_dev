Dictionary<string, string> requestState = new Dictionary<string, string>();
Item oldValuesQuery = this.newItem("cmf_PropertyType", "get");
oldValuesQuery.setID(this.getID());
oldValuesQuery.setAttribute("select", "default_permission,source_id");
oldValuesQuery = oldValuesQuery.apply();
string documentElementTypeId = oldValuesQuery.getProperty("source_id");

Item documentTypeQuery = this.newItem("cmf_ElementType", "get");
documentTypeQuery.setID(documentElementTypeId);
documentTypeQuery.setAttribute("select", "source_id");
documentTypeQuery = documentTypeQuery.apply();
string documentTypeId = documentTypeQuery.getProperty("source_id");

if (!RequestState.Contains(documentTypeId))
{
    return this.getInnovator().newError("It's impossible to save certain Element Type or Property Type. Instead save entire Content Type item.");
}

if (this.node.SelectSingleNode("default_permission") != null)
{
	string oldPermission = oldValuesQuery.getProperty("default_permission");
	if (oldPermission != this.getProperty("default_permission"))
	{
		requestState["default_permission"] = oldPermission;
	}
}

if (requestState.Count > 0)
{
    RequestState[this.getID()] = requestState;
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_PropertyBeforeUpdate' and [Method].is_current='1'">
<config_id>3FE55D65195F43818A3A9DF9F10B979F</config_id>
<name>cmf_PropertyBeforeUpdate</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
