/*
目的:一般的工時卡,更新時自動計算結束時間等於開始時間+工時,APP打卡的,則忽略計算
*/
string strMethodName = "In_SetTimeRecordEndTime";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{
    if(this.getProperty("in_is_app_created","0")=="0")
    {
        double dblHours = Double.Parse(this.getProperty("in_work_hours","0"));
        DateTime dtStartDateTime = System.DateTime.Parse(this.getProperty("in_start_time"));
        DateTime dtEndDateTime = dtStartDateTime.AddHours(dblHours);
        sql = "Update In_TimeRecord set in_end_time='" + dtEndDateTime.ToString("yyyy-MM-ddTHH:mm:ss") + "' where id='"  + this.getID() + "'";
        inn.applySQL(sql);
    }
	else
	{
		//若是APP打卡,則透過開始結束時間計算工時
		DateTime dtStartDateTime = System.DateTime.Parse(this.getProperty("in_start_time"));
		string strEndDateTime = this.getProperty("in_end_time","");
		if(strEndDateTime!="")
		{
			DateTime dtEndDateTime = System.DateTime.Parse(strEndDateTime);
			TimeSpan ts = new TimeSpan(dtEndDateTime.Ticks - dtStartDateTime.Ticks);
			double intMinutes =Math.Round(ts.TotalMinutes,0,MidpointRounding.AwayFromZero);
			double dblHours = Math.Round(intMinutes/30,0);
			dblHours = Math.Round(dblHours/2,0);
			sql = "Update In_TimeRecord set in_work_hours=" + dblHours.ToString() + ",in_work_minutes=" + intMinutes.ToString() + " where id='"  + this.getID() + "'";
			inn.applySQL(sql);
		}
	}
	

    if(this.getProperty("in_workorder")!=null)
    {
        sql = "SELECT SUM(in_today_hours)AS[in_today_hours]FROM[In_TimeRecord] WHERE[in_workorder]='"+this.getProperty("in_workorder")+"'";
        Item itmSQL = inn.applySQL(sql);

        sql = "UPDATE[In_WorkOrder]SET[in_act_total]='"+itmSQL.getProperty("in_today_hours")+"' WHERE[ID]='"+this.getProperty("in_workorder")+"'";
        inn.applySQL(sql);
    }

    if(this.getProperty("in_activity2")!=null)
    {
        sql = "SELECT SUM(in_today_hours)AS[in_today_hours]FROM[In_TimeRecord] WHERE[in_activity2]='"+this.getProperty("in_activity2")+"'";
        Item itmSQL = inn.applySQL(sql);

        sql = "UPDATE[Activity2]SET[actual_work]='"+itmSQL.getProperty("in_today_hours")+"' WHERE[ID]='"+this.getProperty("in_activity2")+"'";
        inn.applySQL(sql);
    }
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
		

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetTimeRecordEndTime' and [Method].is_current='1'">
<config_id>2303FA7E3CA14C9E93CD8190C8E26022</config_id>
<name>In_SetTimeRecordEndTime</name>
<comments>一般的工時卡,更新時自動計算結束時間等於開始時間+工時,APP打卡的,則忽略計算</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
