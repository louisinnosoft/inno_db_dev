var dateFromValue = getRelationshipProperty(relationshipID, "date_from"),
	dateToValue = getRelationshipProperty(relationshipID, "date_to");

if (dateFromValue && dateToValue) {
	dateFrom = Date.parse(dateFromValue);
	dateTo = Date.parse(dateToValue);

	if (dateFrom > dateTo) {
		switch (propertyName) {
			case "date_to":
				if (top.aras.confirm(top.aras.getResource("project", "project_tree.can_not_change_to_negative_duration"))) {
					setTimeout(function () {
						setRelationshipProperty(relationshipID, "date_to", "");
						gridApplet.edit_Experimental.set(relationshipID, colNumber);
					}, 0);
					return true;
				}
				else {
					setRelationshipProperty(relationshipID, "date_to", "");
					return false;
				}
				break;
			case "date_from":
				setRelationshipProperty(relationshipID, "date_to", dateFromValue);
				break;
		}
	}
}

return true;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TimeRecord onDateChange' and [Method].is_current='1'">
<config_id>61E9C9925D40422793973B4CCA93A789</config_id>
<name>TimeRecord onDateChange</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
