//"OnAfterNew" Event (Client Events tab)
//debugger;

var now = new Date();
var date = now.format("yyyy-MM-dd");
this.setProperty("in_date",date);

if ("" === this.getProperty("in_quote_template",""))
{
	var aml = "<AML>";
	aml += "<Item type='In_Quote' action='get' select='id'>";
	aml += "<item_number>SYS-001</item_number>";
	aml += "</Item></AML>";
	var itmQuoteTemp = this.getInnovator().applyAML(aml);
	if(!itmQuoteTemp.isError())
	{
	this.setProperty("in_quote_template",itmQuoteTemp.getID());
	}
}
if ("" === this.getProperty("in_sales",""))
{
	var Inno_createdByID = this.getInnovator().getUserID();
	this.setProperty("in_sales",Inno_createdByID);
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_QuoteInit_C' and [Method].is_current='1'">
<config_id>DBE547B17C5C417DB2E2EE3939B8FB87</config_id>
<name>In_QuoteInit_C</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
