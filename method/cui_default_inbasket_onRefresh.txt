var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onRefreshCommand) {
	workerFrame.onRefreshCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_onRefresh' and [Method].is_current='1'">
<config_id>DBCEA00FBF854ABEAF0737B75BC34B0A</config_id>
<name>cui_default_inbasket_onRefresh</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
