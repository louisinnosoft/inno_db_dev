/*
目的:從專案產生一個對應的預算表
做法:
1.建立一個預算表
2.
*/

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";

Item itmNewBudget = null;
Item itmBudgetTemplate = null;

DateTime dtNowDateTime = DateTime.Now;
string strNowYear = dtNowDateTime.ToString("yyyy");

try
{
    if(this.getProperty("in_company","")=="")
    {
        return this;
    }

    if(this.getProperty("in_temp_budget","")!="")
    {
        aml  = "<AML>";
        aml += "<Item type='In_Budget' action='get'>";
        aml += "<id>"+this.getProperty("in_temp_budget","")+"</id>";
        aml += "<in_company>"+this.getProperty("in_company","")+"</in_company>";
        // aml += "<in_year>"+strNowYear+"</in_year>";
        aml += "</Item></AML>";
        itmBudgetTemplate = inn.applyAML(aml);
        if(itmBudgetTemplate.isError())
        {
            
            throw new Exception("選擇了非與本專案有關的預算表範本");
        }

        aml  = "<AML>";
        aml += "<Item type='In_Budget' action='get'>";
        aml += "<in_refproject>"+this.getID()+"</in_refproject>";
        aml += "</Item></AML>";
        itmNewBudget = inn.applyAML(aml);
        if(itmNewBudget.isError())
        {
            itmNewBudget = inn.newItem("In_Budget","add");
            itmNewBudget.setProperty("in_proposal",this.getProperty("in_from_item",""));
            itmNewBudget.setProperty("in_name",this.getProperty("name",""));
            itmNewBudget.setProperty("in_refproject",this.getProperty("id",""));
            itmNewBudget.setProperty("owned_by_id",this.getProperty("owned_by_id",""));
            itmNewBudget.setProperty("in_company",this.getProperty("in_company",""));
            itmNewBudget.setProperty("item_number",this.getProperty("project_number",""));
            itmNewBudget.setProperty("in_temp_project",this.getProperty("from_template",""));
            itmNewBudget.setProperty("in_year",strNowYear);
            itmNewBudget.setProperty("team_id",this.getProperty("team_id",""));
            itmNewBudget = itmNewBudget.apply();

            aml  = "<AML>";
            aml += "<Item type='ItemType' action='get'>";
            aml += "<name>In_Budget</name>";
            aml += "</Item></AML>";
            Item itmItemType = inn.applyAML(aml);

            aml  = "<AML>";
            aml += "<Item type='RelationshipType' action='get'>";
            aml += "<source_id>"+itmItemType.getID()+"</source_id>";
            aml += "<hide_in_all>0</hide_in_all>";
            aml += "</Item></AML>";
            Item itmRelationshipType = inn.applyAML(aml);
            for(int i=0;i<itmRelationshipType.getItemCount();i++)
            {
                aml  = "<AML>";
                aml += "<Item type='"+itmRelationshipType.getItemByIndex(i).getProperty("name","")+"' action='get'>";
                aml += "<source_id>"+itmBudgetTemplate.getID()+"</source_id>";
                aml += "</Item></AML>";
                Item itmTemplate = inn.applyAML(aml);
                for(int j=0;j<itmTemplate.getItemCount();j++)
                {
                    Item itmNewItem = inn.newItem(itmTemplate.getItemByIndex(j).getType(),"add");

                    if(itmTemplate.getItemByIndex(j).getProperty("related_id","")!="")
                    {
                        Item itmRelated = inn.getItemById(itmTemplate.getItemByIndex(j).getPropertyAttribute("related_id","type",""),itmTemplate.getItemByIndex(j).getProperty("related_id",""));

                        itmNewItem.setRelatedItem(itmRelated);
                    }

                    aml  = "<AML>";
                    aml += "<Item type='Property' action='get'>";
                    aml += "<source_id>"+itmRelationshipType.getItemByIndex(i).getProperty("relationship_id","")+"</source_id>";
                    aml += "<is_hidden2>0</is_hidden2>";
                    aml += "</Item></AML>";
                    Item itmProperty = inn.applyAML(aml);
                    for(int k=0;k<itmProperty.getItemCount();k++)
                    {
                        itmNewItem.setProperty(itmProperty.getItemByIndex(k).getProperty("name",""),itmTemplate.getItemByIndex(j).getProperty(itmProperty.getItemByIndex(k).getProperty("name",""),""));
                    }

                    itmNewBudget.addRelationship(itmNewItem);
                    itmNewBudget = itmNewBudget.apply();
                }
            }
        }
    }
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml  + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmNewBudget;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ProjectToBudget_S' and [Method].is_current='1'">
<config_id>BDED54460AAA49F6AF09901CF0A25280</config_id>
<name>In_ProjectToBudget_S</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
