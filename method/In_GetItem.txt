//System.Diagnostics.Debugger.Break();
//In_GetItem
Innovator inn = this.getInnovator();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
 Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
 Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string[] arrCriteriaSec = criteria.Split('&'); //criteria = "itemtype:In_WorkflowForm&itemid:attachedId&app:in_web_view";
string aml = "";
string sql = "";
string strNewCriteria = criteria;
Item itmProject  = null;
Item itmWFF=null;
if(arrCriteriaSec[0].Split(':')[1] == "In_WorkflowForm")
{
	aml = "<AML>";
	aml += "<Item type='In_WorkflowForm' action='get' id='" + arrCriteriaSec[1].Split(':')[1] + "'>";
	aml += "</Item></AML>";

	itmWFF = inn.applyAML(aml);

	strNewCriteria = "itemtype:" + itmWFF.getProperty("in_itemtype") + "&itemid:" + itmWFF.getProperty("in_item_id") + "&app:in_web_view";
}

try
{
	r = _InnoApp.GetItem(strNewCriteria);

	/*處理打卡
	A.如果是In_WorkOrder
	1.必須要是 In_Task_Execution 名稱的 任務才允許打卡
	1.依據本人，iscurrent找到對應的in_workorder_detail, 如果沒有，代表他不需要打卡
	2.依據in_workorder_detail_type+ in_workorder_detail_id + identity+有開始沒結束 找到對應的 timerecord
	3.如果沒找到timerecord代表沒有打卡，所以要回傳空白的timerecord id, 否則就回傳找到的timerecord

	B.如果是In_Equipment
	*/
	Item itmTimeRecords=null;	
	string strItemType = _InnoApp.GetParameterValue(strNewCriteria,"itemtype",'&',':');
	string strItemId = _InnoApp.GetParameterValue(strNewCriteria,"itemid",'&',':');
	string strother_timerecord_name="";
	switch(strItemType)
	{
		case "In_WorkOrder":
		    if(itmWFF!=null)
		    {
		       if(itmWFF.getProperty("in_current_activity_name","")=="In_Task_Execution" || itmWFF.getProperty("in_current_activity_name","")=="Task execution")
			{
				string strLoginName = _InnoApp.GetUserInfo(UserInfo, "loginid");
				strLoginName = strLoginName.Replace(" ","");			
				Item itmLoginIdentity = _InnH.GetIdentityByUserLoginName(strLoginName);
				aml = "<AML>";
				aml += "<Item type='In_WorkOrder_Detail' action='get'>";
				aml += "<source_id>" + strItemId + "</source_id>";
				aml += "<in_new_assignment>" + itmLoginIdentity.getID() + "</in_new_assignment>";
				aml += "<in_is_current>1</in_is_current>";
				aml += "</Item></AML>";
				Item itmWODetail = inn.applyAML(aml);
				if(!itmWODetail.isError())
				{
					aml = "<AML>";
					aml += "<Item type='In_TimeRecord' action='get' orderBy='in_start_time DESC'>";
					aml += "<in_workorder_detail>" +  itmWODetail.getID() + "</in_workorder_detail>";
					aml += "<owned_by_id>" + itmLoginIdentity.getID() + "</owned_by_id>";
					aml += "<in_end_time condition='is null'></in_end_time>";			
					aml += "</Item></AML>";			
					itmTimeRecords = inn.applyAML(aml);				
				}

				//要抓這個人的其他打卡單據

				aml = "<AML>";
				aml += "<Item type='In_TimeRecord' action='get' orderBy='in_start_time DESC'>";			
				aml += "<owned_by_id>" + itmLoginIdentity.getID() + "</owned_by_id>";
				if(itmTimeRecords!=null)
				{
					if(!itmTimeRecords.isError())
					{
						aml += "<id condition='ne'>" + itmTimeRecords.getItemByIndex(0).getID() + "</id>";
					}
				}
				aml += "<in_end_time condition='is null'></in_end_time>";			
				aml += "</Item></AML>";			
				Item itmOtherTimeRecords = inn.applyAML(aml);	
				if(!itmOtherTimeRecords.isError())
				{
					strother_timerecord_name = itmOtherTimeRecords.getItemByIndex(0).getPropertyAttribute("in_workorder","keyed_name","");
					sql = "select in_name from [in_workorder_detail] where id='" + itmOtherTimeRecords.getItemByIndex(0).getProperty("in_workorder_detail","") + "'";
					Item itmWorkorderDetail = inn.applySQL(sql);
					strother_timerecord_name += "-" + itmWorkorderDetail.getProperty("in_name");					
				}			
			} 
		    }

			break;
		case "In_Equipment":
				aml = "<AML>";
				aml += "<Item type='In_TimeRecord' action='get'>";
				aml += "<in_equipment>" +  strItemId + "</in_equipment>";
				aml += "<in_end_time condition='is null'></in_end_time>";			
				aml += "</Item></AML>";			
				itmTimeRecords = inn.applyAML(aml);		

				aml = "<AML>";
				aml += "<Item type='Project' action='get' select='id,name'>";
				aml += "<state>Active</state>";
				aml += "</Item>";
				aml += "</AML>";
				itmProject = inn.applyAML(aml);
				if(itmProject.isError())
					itmProject=null;


			break;
		default:
			break;
	}
	if(itmTimeRecords!=null){

		string strTimeRecordId = "";
		//允許結束日與開始日不同天,因為有夜班,而且算錢的METHOD也只看開始日
		if(!itmTimeRecords.isError())
			strTimeRecordId = itmTimeRecords.getItemByIndex(0).getID();

		r = Innosoft.InnUtility.AppendXmlNode(r,"result","<timerecord_id>" + strTimeRecordId + "</timerecord_id><other_timerecord_name>" + strother_timerecord_name + "</other_timerecord_name>");
	}

	if(itmProject!=null)
	{
		r = Innosoft.InnUtility.AppendXmlNode(r,"result","<projects>" + itmProject.dom.SelectSingleNode("//Result").InnerXml + "</projects>");
	}



}
catch (Exception ex)
{
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


return  inn.newResult(r);	
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetItem' and [Method].is_current='1'">
<config_id>18BC755451E5425E880D31ECBE6160BB</config_id>
<name>In_GetItem</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
