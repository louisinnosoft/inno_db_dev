
Innovator inn=this.getInnovator();
Item rst=inn.newItem();
string meeting_id=this.getProperty("meeting_id","CB12D9C4F9434AA7A7BC74A2F499855B");
string surveytype=this.getProperty("surveytype","");

if(meeting_id=="no_data"||surveytype=="no_data"){
    return inn.newError("Parameter not received meeting_id =  "+meeting_id+"   surveytype = "+surveytype);
}
Item meeting=inn.getItemById("In_Meeting",meeting_id);

Item meetingSurveys=inn.newItem();
meetingSurveys.setAttribute("type","In_Meeting_Surveys");
meetingSurveys.setAttribute("action","get");
meetingSurveys.setProperty("source_id",meeting_id);
meetingSurveys.setProperty("in_surveytype",surveytype);
meetingSurveys=meetingSurveys.apply();
int mssCount=meetingSurveys.getItemCount();
for(int s=0;s<mssCount;s++){
    rst.addRelationship(meetingSurveys.getItemByIndex(s));
}


rst.setProperty("inn_surveytype",surveytype);
Item meetingStatistics=inn.newItem();
meetingStatistics.setAttribute("type","In_Meeting_Surveys_statistics");
meetingStatistics.setAttribute("action","get");
meetingStatistics.setProperty("source_id",meeting_id);
meetingStatistics.setProperty("in_surveytype",surveytype);
meetingStatistics=meetingStatistics.apply();

int mtsCount=meetingStatistics.getItemCount();
for(int xk=0;xk<mtsCount;xk++){
    Item tmp=meetingStatistics.getItemByIndex(xk);
    tmp.setProperty("inn_surveytype",tmp.getRelatedItem().getProperty("in_surveytype"));
    tmp.setProperty("inn_question_no",tmp.getRelatedItemID());
    rst.addRelationship(tmp);
}
return rst;
//return inn.newResult(rst.dom.InnerXml);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingStatisticsSheet' and [Method].is_current='1'">
<config_id>7D037CA133DB4A1D9DDF9CF3763AE439</config_id>
<name>In_Get_MeetingStatisticsSheet</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
