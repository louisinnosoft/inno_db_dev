/*
目的:從相同Itemtype的Template謄寫屬性與關聯
位置:OnAfterAdd
作法:
1.設定好要謄寫的屬性與關聯
1.取得AML Context中的全部欄位,作為Template比對的欄位
2.依據比對欄位找到Template
3.透過底層功能謄寫
關聯Method:
記得要一併修改 Project_CloneProjectOrTemplate 內,江Deliverable的 in_is_template 設定為 0
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
strMethodName = strMethodName.Replace("EventArgs)","");

string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmR=null;


try
{
	string strMappingProperties = "in_doc_type";
	itmR = this.apply("In_GetNextNumber");


    if(this.getProperty("in_is_template","")=="1")
    	return this;

    if(this.getProperty("in_is_project_clone","")=="1")
    	return this;



	Item itmNewDoc = _InnH.CloneItemFromMatchedTemplate(itmR,itmR.getType(),strMappingProperties,"item_number,name,in_company");
	if(itmNewDoc.isError())
	{
		if(itmNewDoc.getErrorCode()!="NoTemplate")
			throw new Exception(itmNewDoc.getErrorString());
	}
	else
	{
		itmR = itmNewDoc;
	}




}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml ;
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");

	return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Document_CloneFromTemplate_S' and [Method].is_current='1'">
<config_id>6F86005B708C42558E7BED0F850C8C33</config_id>
<name>In_Document_CloneFromTemplate_S</name>
<comments>從相同Itemtype的Template謄寫屬性與關聯</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
