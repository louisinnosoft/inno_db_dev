// PE_CreateNewRevision
// Called from a server action. Creates a new revision outside of change control

Innovator inn = this.getInnovator();
string thisType = this.getType();
string thisName = this.getProperty("keyed_name","");

if(this.isNew() || this.getAttribute("isTemp", "").Equals("1", StringComparison.OrdinalIgnoreCase))
{
  return inn.newError(CCO.ErrorLookup.Lookup("PE_CreateNewRevision_MustBeSaved", thisType));
}

bool isInReleasedState = String.Equals(this.getProperty("state",""), "Closed", StringComparison.Ordinal);

// Item must be in the Released state
if (!isInReleasedState)
{
  return inn.newError(CCO.ErrorLookup.Lookup("PE_ManualRelease_MustBeInReleasedState", thisType));
}

// Make sure the current user is in the Owner identity
string ownedById = this.getProperty("owned_by_id");
if (!CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, ownedById))
{
  return inn.newError(CCO.ErrorLookup.Lookup("PE_ManualRelease_YouMustBeAMember", this.getPropertyAttribute("owned_by_id","keyed_name","Owner")));
}

// Make sure the item is not used on any change items
Item checkResult = this.apply("PE_CheckChangeItemUsage");
if(checkResult.isError())
{
  return checkResult;
}
  
// Grant 'Aras PLM' permissions
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
  // Version and unlock the item
  Item resItem = this.apply("version");
  if (resItem.isError()) return resItem;
  resItem = resItem.apply("unlock"); 
  return resItem; 
}
finally
{
  // Revoke 'Aras PLM' permissions
  if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_CreateNewRevision' and [Method].is_current='1'">
<config_id>487CFDDF4850492288711BC61CEED00A</config_id>
<name>PE_CreateNewRevision</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
