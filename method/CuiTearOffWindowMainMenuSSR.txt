	Item items = null;

	//ItemType and Item reports
	string itemTypeId = this.getProperty("item_type_id");
	if (!string.IsNullOrEmpty(itemTypeId))
	{
		//Sel-Service Reports
		Item userReports = this.newItem("Method", "GetSelfServiceReports");
		userReports.setProperty("base_item_type", itemTypeId);
		userReports = userReports.apply();
		if (!userReports.isError())
		{
			items = HandleMenuButtons(userReports, "userreport:{0}");
		}
	}

	return items ?? this.getInnovator().newResult("");
}

private static Item HandleMenuButtons(Item reports, string reportTemplate)
{
	int reportsCount = reports.getItemCount();
	if (reportsCount > 0)
	{
		Item cuiItems = reports.newItem();
		for (var i = 0; i < reportsCount; i++)
		{
			Item currentReport = reports.getItemByIndex(i);
			Item menuButton = currentReport.newItem("CommandBarMenuButton");
			string reportId = currentReport.getID();
			string reportName = string.Format(CultureInfo.InvariantCulture, reportTemplate, reportId);
			menuButton.setProperty("name", reportName);
			menuButton.setProperty("label", currentReport.getProperty("label") ?? currentReport.getProperty("name"));

			// 'ABC76A89AB8B48F6BA8AD121247D7B88' - com.aras.innovator.cui_default.twmm_reports_menu
			menuButton.setProperty("parent_menu", "ABC76A89AB8B48F6BA8AD121247D7B88");
			menuButton.setProperty("action", "Add");
			menuButton.setProperty("sort_order", "0");
			menuButton.setID(reportId);
			menuButton.removeAttribute("isNew");
			menuButton.removeAttribute("isTemp");
			cuiItems.appendItem(menuButton);
		}

		// newItem() is creates empty node. It's should be deleted
		cuiItems.removeItem(cuiItems.getItemByIndex(0));
		return cuiItems;
	}

	return null;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CuiTearOffWindowMainMenuSSR' and [Method].is_current='1'">
<config_id>4B2A5D2095B24A90B01BD56D0B57E418</config_id>
<name>CuiTearOffWindowMainMenuSSR</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
