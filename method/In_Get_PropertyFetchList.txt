



Innovator inn=this.getInnovator();
string typeToFetch=this.getProperty("typeToFetch","no_data");
string fPropName=this.getProperty("fetch_property_name","name");//要將哪個屬性值傳回
string datatypeToFetch=this.getProperty("datatype","item");
string fetchingTypesCSV="";
if(typeToFetch=="no_data"){return inn.newError("missing key parameter: typeToFetch");}

Item itmQ=inn.newItem("ItemType","get");
itmQ.setProperty("name",typeToFetch);
itmQ=itmQ.apply();

Item properties=inn.newItem("Property","get");
properties.setProperty("source_id",itmQ.getID());
properties=properties.apply();

int propCount=properties.getItemCount();


for(int x=0; x<propCount;x++){
    Item tmp=properties.getItemByIndex(x);
    if(tmp.getProperty("data_type","")==datatypeToFetch){
        fetchingTypesCSV+=tmp.getProperty(fPropName)+",";
    }
}

return inn.newResult(fetchingTypesCSV.TrimEnd(','));
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_PropertyFetchList' and [Method].is_current='1'">
<config_id>74A3D04903674F8CAAD329640E22ECC7</config_id>
<name>In_Get_PropertyFetchList</name>
<comments>取得需要向下抓一層的屬性，以CSV回傳清單。內部使用。主要由In_Search_ItemWithParameters呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
