string id = this.getID();

if (String.IsNullOrEmpty(id))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_GetMessagesForFMGIdNotSet"));
}
Item fmg = GetFMG(id);
if (fmg.isError())
{
	throw new Exception(String.Format(CultureInfo.InvariantCulture, CCO.ErrorLookup.Lookup("SSVC_GetMessagesForFMG"), fmg.getErrorString()));
}
Item aml = GetFMGAml(fmg, CCO);
Item filter = this.getPropertyItem("filter");
if (filter != null)
{
	aml = MergeRequestItems(aml, filter);
}

var fmgAml = ApplyFMGAml(aml);

string userID = fmg.getProperty("user_criteria_id");
fmgAml = AddPrivateMessages(userID, CCO, filter, fmgAml);
return fmgAml;
}

private static Item MergeLists(Item source, Item toAdd) {
	var sourceArr = new List<string>();
	for (var i = 0; i < source.getItemCount(); i++) {
		var tempItem = source.getItemByIndex(i);
		sourceArr.Add(tempItem.getID());
	}

	for (var i = 0; i < toAdd.getItemCount(); i++) {
		var itemToAdd = toAdd.getItemByIndex(i);
		if (!sourceArr.Contains(itemToAdd.getID())) {
			source.appendItem(itemToAdd);
		}
	}

	return source;
}

private Item AddPrivateMessages(string userID, Aras.Server.Core.CallContext CCO, Item filter, Item fmgAml)
{
    Item userPrivateMessages = GetPrivateMessages(userID);
    if(!userPrivateMessages.isError())
    {
        Item userPrivateAml = GetFMGAml(userPrivateMessages, CCO);
        if (filter != null)
        {
        	userPrivateAml = MergeRequestItems(userPrivateAml, filter);
        }
        var userMessages = ApplyFMGAml(userPrivateAml);
        if (!userMessages.isNew()) {
            if(String.IsNullOrEmpty(fmgAml.getResult()))
            {
                fmgAml = userMessages;
            }
            else
            {
                fmgAml = MergeLists(fmgAml, userMessages);
            }
        }
    }
    return fmgAml;
}

private Item ApplyFMGAml(Item aml)
{
	Item item;
	try
	{
		item = aml.apply();
	}
	catch
	{
		item = this.getInnovator().newResult("");
	}
	return item;
}

private Item MergeRequestItems(Item aml, Item filter)
{
	XmlNode target = aml.node;
	foreach (XmlNode child in filter.node.ChildNodes)
	{
		XmlNode node2Copy = aml.dom.ImportNode(child, true);
		target.AppendChild(node2Copy);
	}
	return aml;
}

private Item GetFMGAml(Item fmg, Aras.Server.Core.CallContext CCO)
{
	Item item = this.newItem("", "");
	string aml;
	string group_type = fmg.getProperty("group_type").ToUpperInvariant();
	if (group_type == "USERMESSAGES") {
		string userId = fmg.getProperty("user_criteria_id");
		aml = GetAmlForCreatedAndFlaggedMessagesByUser(userId);
	} else {
		aml = fmg.getProperty("aml");
		if (String.IsNullOrEmpty(aml))
		{
			throw new Exception(CCO.ErrorLookup.Lookup("SSVC_GetMessagesForFMGAmlIsEmpty"));
		}
	}

	item.loadAML(aml);
	return item;
}

private static string GetAmlForCreatedAndFlaggedMessagesByUser(string userId)
{
	const string amlForCreatedAndFlaggedMessagesByUser = "<Item type=\"SecureMessage\" select='id,thread_id,disabled_on,disabled_by_id,created_on,created_on_tick,created_by_id(login_name),comments,classification,item_major_rev,item_version,item_state,item_keyed_name,item_type_id,item_type_name,item_id,item_config_id' action=\"get\" where=\"([SECUREMESSAGE].created_by_id = '{0}' and [SECUREMESSAGE].classification != 'History') or [SECUREMESSAGE].id in (select source_id from [SECUREMESSAGEFLAGGEDBY] as f where f.flagged_by_id = '{0}')\"><disabled_by_id condition=\"is null\"/></Item>";

	return string.Format(CultureInfo.InvariantCulture, amlForCreatedAndFlaggedMessagesByUser, userId);
}

private Item GetPrivateMessages(string userID)
{
	Item item = this.newItem("ForumMessageGroup", "get");
	item.setProperty("user_criteria_id", userID);
	item.setProperty("group_type", "UserPrivateMessages");
	return item.apply();
}

private Item GetFMG(string id)
{
	Item item = this.newItem("ForumMessageGroup", "get");
	item.setID(id);
	return item.apply();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_GetFMGMessages' and [Method].is_current='1'">
<config_id>09FFAA1D758740A68E968BACA2181720</config_id>
<name>VC_GetFMGMessages</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
