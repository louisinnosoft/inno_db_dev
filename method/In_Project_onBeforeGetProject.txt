
Dim project As XmlElement = Me.dom.SelectSingleNode("//Item")
Dim typeId As String
Dim selectStr As String = project.GetAttribute("select")
If Not project Is Nothing Then typeId = project.GetAttribute("typeId")

If Not (selectStr.Equals("*") Or selectStr.Equals("")) Then
    If typeId = "" Then
	    Dim ItemType As XmlElement = CCO.Cache.GetItemTypeFromCache("Project", "name")
	    typeId = ItemType.GetAttribute("id")
    End If
	
    For Each curProperty As XmlElement In CCO.Cache.GetPropertiesFromCache(typeId)
    	Dim propName As String = curProperty.GetAttribute("name")
    	If propName.StartsWith("in_status_") AndAlso curProperty.GetAttribute("data_type") = "color list" Then
		    selectStr = selectStr & "," &  propName
    	End If
    Next
	selectStr = selectStr & ",css"

	project.SetAttribute("select", selectStr)
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Project_onBeforeGetProject' and [Method].is_current='1'">
<config_id>842D782B7D054D5DB3BFD8C00B600DB7</config_id>
<name>In_Project_onBeforeGetProject</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
