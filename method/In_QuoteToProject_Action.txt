/*
目的:將報價項目轉成專案任務Array,之後透過In_CreateProject產生對應的專案
做法:
*/

// //System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmProject = null;	
try
{
	//先判斷如果一階的數量超過20，不合理，因為不可能有任何影集超過20集，因此擋住，配套方法就是拆成20集一組
	aml = "<AML>";
	aml += "<Item type='In_Quote_Details' action='get'>";
	aml += "<in_level>1</in_level>";
	aml += "<in_qty condition='gt'>20</in_qty>";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	Item itmL1Quote_Details = inn.applyAML(aml);
	if(!itmL1Quote_Details.isError())
	{
		string strError="以下編號的第一階任務數量超過20集，請修正:";
		for(int i=0;i<itmL1Quote_Details.getItemCount();i++)
		{
			Item itmL1Quote_Detail = itmL1Quote_Details.getItemByIndex(i);
			strError += "#" + itmL1Quote_Detail.getProperty("sort_order","0") + ",";
		}
		throw new Exception(strError);
	}


	
	if(this.getProperty("in_project","")=="")
	{
		aml = "<AML>";
		aml += "<Item type='In_Quote_Details' action='get'>";
		aml += "<source_id>" + this.getID() + "</source_id>";
		aml += "</Item></AML>";

		Item itmQuote_Details = inn.applyAML(aml);
		for(int k=0;k<itmQuote_Details.getItemCount();k++)
		{
			Item itmQuote_Detail = itmQuote_Details.getItemByIndex(k);
			sql = "update [In_Quote_Details] set in_config_id='" + inn.getNewID() + "' where id='" + itmQuote_Detail.getID() + "'";
			inn.applySQL(sql);
		}
	}
	
	Item itmR = inn.applyMethod("In_Calculate_Quote_Details_S","<id>" + this.getID() + "</id><reset>1</reset>");
	if(itmR.isError())
		throw new Exception(itmR.getErrorString());
	
	//將文字欄位中的單引號先一律置換成@@@,以免破壞SQL語法,轉換完成後再把@@@置換回單引號
	sql = "Update [In_Quote_Details] set ";
	sql += "in_description=Replace(in_description,'''','@@@')";
	sql += ",in_memo=Replace(in_memo,'''','@@@')";
	sql += ",in_note1=Replace(in_note1,'''','@@@')";
	sql += ",in_note2=Replace(in_note2,'''','@@@')";
	sql += ",in_part_name=Replace(in_part_name,'''','@@@')";
	sql += " where source_id='" + this.getID() + "'";
	inn.applySQL(sql);
	
	try{
	//itmProject = _InnH.QuoteToProject(this.getID());
	itmProject = inn.applyMethod("In_QuoteToProject","<QuoteId>" + this.getID() + "</QuoteId>");
	}
	catch(Exception ex1)
	{
	    
		throw new Exception("本報價結構與前版專案結構差異不匹配,系統無法比對更新");
	}
	
	sql = "Update [In_Quote_Details] set ";
	sql += "in_description=Replace(in_description,'@@@','''')";
	sql += ",in_memo=Replace(in_memo,'@@@','''')";
	sql += ",in_note1=Replace(in_note1,'@@@','''')";
	sql += ",in_note2=Replace(in_note2,'@@@','''')";
	sql += ",in_part_name=Replace(in_part_name,'@@@','''')";	
	sql += " where source_id='" + this.getID() + "'";
	inn.applySQL(sql);
	
	sql = "Update [Activity2] set ";
	sql += "[name]=Replace([name],'@@@','''')";	
	sql += ",[in_note1]=Replace([in_note1],'@@@','''')";	
	sql += ",[in_note2]=Replace([in_note2],'@@@','''')";	
	sql += " where proj_num=" + itmProject.getProperty("project_number");
	inn.applySQL(sql);
	
	sql = "Update [WBS_Element] set ";
	sql += "[name]=Replace([name],'@@@','''')";	
	sql += ",[in_note1]=Replace([in_note1],'@@@','''')";	
	sql += ",[in_note2]=Replace([in_note2],'@@@','''')";	
	sql += " where proj_num=" + itmProject.getProperty("project_number");
	inn.applySQL(sql);
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(_InnH.Translate(ex.Message));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmProject;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_QuoteToProject_Action' and [Method].is_current='1'">
<config_id>8616AF5A48BD46DF9392DEF94D1BF22B</config_id>
<name>In_QuoteToProject_Action</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
