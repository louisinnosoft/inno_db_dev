//System.Diagnostics.Debugger.Break();

string strFromRelName = this.getProperty("FromRelName");
string strToRelName = this.getProperty("ToRelName");
string strCopyProperies = this.getProperty("CopyProperies");
string strFromSourceID = this.getProperty("FromSourceID");
string strToSourceID = this.getProperty("ToSourceID");
string strFromSourceName = this.getProperty("FromSourceName");
string strToSourceName = this.getProperty("ToSourceName");

string r= "";

Innovator inn = this.getInnovator();

Item itmFromSource = inn.getItemById(strFromSourceName,strFromSourceID);
Item itmToSource = inn.getItemById(strToSourceName,strToSourceID);

Item itmFromRels = inn.newItem(strFromRelName,"get");
itmFromRels.setProperty("source_id",strFromSourceID);
itmFromRels = itmFromRels.apply();

string[] arrCopyProperies = strCopyProperies.Split(',');

for(int i=0;i<itmFromRels.getItemCount();i++)
{
	Item itmFromRel = itmFromRels.getItemByIndex(i);
	Item itmToRel = inn.newItem(strToRelName,"add");
	for(int j=0;j<arrCopyProperies.Length;j++)
	{
		itmToRel.setProperty(arrCopyProperies[j],itmFromRel.getProperty(arrCopyProperies[j],""));		
	}
	itmToRel.setProperty("source_id",strToSourceID);
	itmToRel = itmToRel.apply();	
	r += itmToRel.getID() + ",";
}

return inn.newResult(r);


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CopyRelationships' and [Method].is_current='1'">
<config_id>EB36C21DD5EE40ED8F1B29690C5B4A75</config_id>
<name>In_CopyRelationships</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
