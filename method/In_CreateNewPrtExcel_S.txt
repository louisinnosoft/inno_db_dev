//System.Diagnostics.Debugger.Break();
/*
目的:依據Classification產生對應的 QR Code,並填入EXCEL中
做法:
1.產生QR Code,並另存成file
2.將QR Code檔案上傳至Vault中
3.找到範本 Excel,放入in_file內
4.產生一個QUEUE,直接轉檔
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml="";
string sql = "";
string param="";
string str_r="";

if(this.getLockStatus()!=0)
	throw new Exception("請先解鎖本文件再執行");


Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	//1.產生QR Code,並另存成file
	string strQRCodePath = CCO.Server.MapPath("../server/temp/" + this.getID() + ".jpg");
	Item itmAppUrl = inn.getItemByKeyedName("In_Variable","app_url");
	if(itmAppUrl==null)
		throw new Exception(_InnH.Translate("查無變數:[app_url],請檢查是否有設定app網址"));
	string strAppUrl = itmAppUrl.getProperty("in_value","");
	strAppUrl = strAppUrl + "?redir=Controls/ActVote.htm?itemtype=" + this.getType() + ":itemid=" + this.getID();
	
	Innosoft.InnUtility.GenerateQRCode(strAppUrl ,strQRCodePath,300,300,1);		
	
	//2.將QR Code檔案上傳至Vault中
	Item itmQRCode = inn.newItem("File","add");
	//itmQRCode.setProperty("filename","QRCode-" + this.getType() + "-" + this.getID() + ".jpg");
	itmQRCode.setProperty("filename",this.getProperty("keyed_name","") + ".jpg");
	itmQRCode.attachPhysicalFile(strQRCodePath);
	itmQRCode = itmQRCode.apply();
	
	//3.找到範本 Excel
	aml = "<AML>";
	aml += "<Item type='In_NewPrt_process' action='get' select='in_file(filename)'>";
	aml += "<in_part_name>Temp-" + this.getProperty("classification","") + "</in_part_name>";
	aml += "</Item></AML>";	
	Item itmTempNewPrtProc = inn.applyAML(aml);	
	if(itmTempNewPrtProc.isError())
		throw new Exception("查無符合本類型的樣板(樣板名稱為:Temp-" + this.getProperty("classification","") + ")");
	
	string strTempExcelFileId = itmTempNewPrtProc.getProperty("in_file","");
	if(strTempExcelFileId=="")
		throw new Exception("樣板名稱為:Temp-" + this.getProperty("classification","") + "無實體檔案");
	
	sql = "Update In_NewPrt_process set in_qrcode='" + itmQRCode.getID() + "'";
	sql += " where id='" + this.getID() + "'";
	Item itmSQL = inn.applySQL(sql);
	
	
	//創建一個Queue
	//創建 In_Queue 的 AML
	string strCreateQueue = "";
	strCreateQueue += "<AML>";
	strCreateQueue += "<Item type='In_Queue' action='add'>";
	strCreateQueue += "<in_source_itemtype>" + this.getType() + "</in_source_itemtype>";
	strCreateQueue += "<in_source_id>" + this.getID() + "</in_source_id>";
	strCreateQueue += "<in_source_keyed_name>@keyed_name</in_source_keyed_name>";
	strCreateQueue += "<in_type>excel_to_excel</in_type>";
	strCreateQueue += "<in_state>start</in_state>";
	strCreateQueue += "<in_inparam><![CDATA[@in_inparam]]></in_inparam>";
	strCreateQueue += "<in_state_method>";
	strCreateQueue += "</in_state_method>";
	strCreateQueue += "<in_priority>2</in_priority>";
	strCreateQueue += "</Item></AML>";
	
	
	param += "<in_param>	";
	param += "<type>excel_to_excel</type>";
	param += "	<body_variable>";
	param += "		<variable name='in_p-in_part_name'>" + this.getProperty("in_part_name","") + "</variable>";
	param += "		<variable name='in_p-part_name_en'>" + this.getProperty("in_part_name_en","") + "</variable>";
	param += "		<variable name='in_p-class'>" + this.getProperty("in_newparttype","") + "</variable>";
	param += "		<variable name='in_p-spec'>" + this.getProperty("in_spec","") + "</variable>";
	param += "		<variable name='in_p-in_part_code'>" + this.getProperty("in_part_code","") + "</variable>";

	param += "		<variable name='in_p-created_on'>" + Convert.ToDateTime(this.getProperty("created_on","")).ToString("yyyy-MM-dd") + "</variable>";
	param += "		<variable name='in_p-created_by_id'>" + this.getPropertyAttribute("created_by_id","keyed_name") + "</variable>";
	param += "		<variable name='in_first_pnumber'>" + this.getProperty("in_first_pnumber","") + "</variable>";
	param += "		<variable name='in_p_number'>" + this.getProperty("in_p_number","") + "</variable>";
	param += "		<variable name='in_p-in_desc'>" + this.getProperty("in_desc","") + "</variable>";

	param += "		<variable name='in_p-in_qrcode' type='file_item' width='70' height='70' offsetx='0' offsety='0'>" + itmQRCode.getID() + "</variable>";
	param += "	</body_variable>";
	//從範本下載Excel
	param += "	<source_itemtype>" + itmTempNewPrtProc.getType() + "</source_itemtype>"; 
	param += "	<source_id>" + itmTempNewPrtProc.getID() + "</source_id>";
	param += "	<source_property>in_file</source_property>";
	//將套表套好的Excel放回主檔
	param += "	<return_itemtype>" + this.getType() + "</return_itemtype>";
	param += "	<return_id>" + this.getID() + "</return_id>";
	param += "	<return_property>in_file</return_property>";
	param += "<return_filename>" + this.getProperty("in_part_name","") + "-" + itmTempNewPrtProc.getPropertyItem("in_file").getProperty("filename","") + "</return_filename>";
	param += "</in_param>";
	
	strCreateQueue = strCreateQueue.Replace("@in_inparam",param);
	strCreateQueue = strCreateQueue.Replace("@keyed_name",this.getProperty("keyed_name"));
	
	Item itmQueue = inn.applyAML(strCreateQueue);
	
	//執行 DocumentHelper 執行  ExcelToExcel
	Innosoft.InnoDocument InnDH = new Innosoft.InnoDocument(inn);
	string xmlResult = InnDH.Process_Excel_to_Excel(itmQueue);
	string state = Innosoft.InnUtility.GetXmlNodeText(xmlResult, "state", "");
	if(state=="false")
		throw new Exception(Innosoft.InnUtility.GetXmlNodeText(xmlResult, "result", ""));
		
}
catch (Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateNewPrtExcel_S' and [Method].is_current='1'">
<config_id>AC6F0BF51F8C4E69BF006242BF820086</config_id>
<name>In_CreateNewPrtExcel_S</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
