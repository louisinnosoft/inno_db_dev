var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame['onShow_clipboardCommand']) {
	workerFrame['onShow_clipboardCommand']();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_show_clipboard' and [Method].is_current='1'">
<config_id>7F81575A55464A9DB44DF86358E115D2</config_id>
<name>cui_default_mwmm_show_clipboard</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
