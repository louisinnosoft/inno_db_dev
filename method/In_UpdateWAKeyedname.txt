Innovator inn =  this.getInnovator();
//System.Diagnostics.Debugger.Break() ;
//Get all Workflow Map Activity Relationship
//Update all the keyed name

string aml = "<AML><Item type='Workflow Map Activity' action='get' select='source_id(keyed_name),related_id(keyed_name,label)'><source_id>";
aml += this.getID() + "</source_id></Item></AML>";

Item wmas = inn.applyAML(aml);

int count = wmas.getItemCount();

for(int i=0;i<count;i++)
{
	Item wma = wmas.getItemByIndex(i);
	Item at = inn.getItemById("Activity Template",wma.getRelatedItemID());
	wma.setProperty("in_keyedname",at.getProperty("label"));
	wma.apply("edit");
}

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateWAKeyedname' and [Method].is_current='1'">
<config_id>6A48020152FF4A4B9A6B3750D0BEF3E9</config_id>
<name>In_UpdateWAKeyedname</name>
<comments>Add By In</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
