XmlDocument variableItemDomFromDB = CCO.GetItem.GetItemById("Variable", this.getID(), 0, "", "value");
XmlNode itm = variableItemDomFromDB.SelectSingleNode("//Item[@type='Variable' and id='" + this.getID() + "']");
if (itm != null)
{
	string oldItemValue = CCO.XML.GetItemProperty(itm, "value");
	if (oldItemValue == null && this.getProperty("value", null) == null)
		this.setProperty("value", this.getProperty("default_value", null));	
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onBeforeUpdateVariable' and [Method].is_current='1'">
<config_id>C1F1B03A84C9422EAABD93C186EF42E5</config_id>
<name>onBeforeUpdateVariable</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
