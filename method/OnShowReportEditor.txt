return ModulesManager.using(['aras.innovator.Izenda/ReportEditorWindowView',
	'aras.innovator.core.ItemWindow/DefaultItemWindowCreator']).then(function(View, Creator) {
		var reportId = aras.getItemProperty(inDom, 'id');
		if (reportId) {
			var isreportSetValid = aras.newIOMItem('Method', 'IsOldReportSet');
			isreportSetValid.setID(reportId);
			isreportSetValid = isreportSetValid.apply();

			if (isreportSetValid.isError() && isreportSetValid.getErrorCode() != '0') {
				aras.AlertError(isreportSetValid.getErrorString());
				return;
			}

			if (isreportSetValid.getResult() == '0') {
				aras.AlertWarning(aras.getResource('../Modules/aras.innovator.Izenda/', 'servicereporting.report_definitions_inconsistencies'));
				return;
			}
		}

		if (inDom.getAttribute('use_custom_form') !== '1') {
			return aras.evalMethod('OnShowItemDefault', inDom, inArgs);
		}

		var existingWnd = aras.uiFindAndSetFocusWindowEx(aras.SsrEditorWindowId);
		if (existingWnd && existingWnd.itemID) {
			reportId = existingWnd.itemID;
			aras.AlertError(aras.getResource('', 'ui_methods_ex.create_only_one_report_at_a_time').replace('{0}', reportId));
			return;
		}

		var view = new View(inDom, inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnShowReportEditor' and [Method].is_current='1'">
<config_id>C845F0921E994C3EBD6C114B5E03C8A1</config_id>
<name>OnShowReportEditor</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
