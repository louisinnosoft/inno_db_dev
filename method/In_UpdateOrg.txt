string strMethodName = "In_UpdateOrg";
/*
目的:更新組織結構
位置:Identity OnAfterUpdate
做法:
1.只處理 組織 類的 identity
2.Merge 更新部門主管 Identity 的 Name
3.ManagerRole 下需有 ManagerMan
4.子階部門Identity 的 dept 都是自己
5.子階Member Identity 的 dept 都是自己, alias 的 owner 都是 ManagerRole,要將個人工時卡的主管換成新主管
6.ChildManagerRole 下需有 ManagerRole	


*/


//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
	bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

string strManagerManId="";
string strManagerRoleId=this.getProperty("in_leaderrole","");
string strParentManagerRoleId="";
string strChildManagerRoleId = "";

	
try
{
	//1.只處理 組織 類的 identity
	if(this.getProperty("in_is_org","")!="1")
		return this;
	
	//2.Merge 更新部門主管 Identity 的 Name
	strManagerManId = this.getProperty("owned_by_id","");
	if(strManagerManId=="")
		throw new Exception("請設定部門主管");
	
	string strDeptManagerName = this.getProperty("name","") + "主管";
	sql = "Update [Identity] set [name]='" + strDeptManagerName + "' where id='" + strManagerRoleId + "'";
	inn.applySQL(sql);
	
	
	//3.ManagerRole 下需有ManagerMan
	aml = "<AML>";
	aml += "<Item type='Member' action='get' select='related_id(is_alias)'>";
	aml += "<source_id>" + strManagerRoleId + "</source_id>";
	aml += "</Item></AML>";
	Item itmManagerMembers = inn.applyAML(aml);
	for(int i=0;i<itmManagerMembers.getItemCount();i++)
	{		
		Item itmManagerMember = itmManagerMembers.getItemByIndex(i);
		if(itmManagerMember.getPropertyItem("related_id").getProperty("is_alias","")=="1")
		{
			sql = "Update [Member] set related_id='" + strManagerManId + "' where id='" + itmManagerMember.getProperty("id")  + "'";
			inn.applySQL(sql);
			break;
		}
	}
		
	
	//4.子階部門Identity 的 dept 都是自己
	aml = "<AML>";
	aml += "<Item type='Identity Structure' action='get' select='related_id(in_leaderrole)'>";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	Item itmChildDepts = inn.applyAML(aml);	
	string strChildDeptIds = _InnH.GetCSV(itmChildDepts,"related_id");
	sql = "Update [Identity] set ";
	sql += "[in_dept] = '" + this.getID() + "'";
	sql += " where id in('" + strChildDeptIds.Replace(",","','") + "')";
	inn.applySQL(sql);

	//5.子階Member Identity 的 dept 都是自己, alias owner 都是 ManagerRole
	aml = "<AML>";
	aml += "<Item type='Member' action='get' select='related_id(is_alias)'>";
	aml += "<source_id>" + this.getID() + "</source_id>";			
	aml += "</Item></AML>";
	Item itmMembers = inn.applyAML(aml);	
	for(int i=0;i<itmMembers.getItemCount();i++)
	{
		Item itmMember = itmMembers.getItemByIndex(i);
		sql = "Update [Identity] set ";
		sql += "[in_dept] = '" + this.getID() + "'";
		if(itmMember.getPropertyItem("related_id").getProperty("is_alias","")=="1")
		{
			sql += ",[owned_by_id] = '" + strManagerManId + "'";
			sql += ",[in_leaderrole] = '" + strManagerRoleId + "'";
		}
		sql += " where id='" + itmMember.getProperty("related_id") + "'";
		inn.applySQL(sql);
		
		//要將個人工時卡的主管換成新主管
		if(itmMember.getPropertyItem("related_id").getProperty("is_alias","")=="1")
		{
			aml = "<AML>";
			aml += "<Item type='In_PerlWorkRecord' action='get'>";
			aml += "<owned_by_id>" + itmMember.getProperty("related_id") + "</owned_by_id>";
			aml += "</Item></AML>";
			Item itmPerlWorkRecord = inn.applyAML(aml);
			if(!itmPerlWorkRecord.isError())
			{
				sql = "Update In_PerlWorkRecord set managed_by_id='" + strManagerRoleId + "' where id='" + itmPerlWorkRecord.getID() + "'";
				inn.applySQL(sql);
			}
		}
		
		
		
	}
	
	
	
	
	
	
	
	//6.ChildManagerRole 下需有 ManagerRole	
	for(int i=0;i<itmChildDepts.getItemCount();i++)
	{
		Item itmChildDept =  itmChildDepts.getItemByIndex(i);
		if(itmChildDept.isError())
			continue;
		strChildManagerRoleId = itmChildDept.getPropertyItem("related_id").getProperty("in_leaderrole","");	
		aml = "<AML>";
		aml += "<Item type='Identity' action='get' select='id,is_alias'>";
		aml += "<id>" + strChildManagerRoleId + "</id>";
		aml += "</Item></AML>";
		Item itmChildManagerRole = inn.applyAML(aml);
		if(itmChildManagerRole.getProperty("is_alias","")=="1")
			continue;
		
		aml = "<AML>";
		aml += "<Item type='Member' action='get' select='related_id(is_alias)'>";
		aml += "<source_id>" + strChildManagerRoleId + "</source_id>";
		aml += "</Item></AML>";
		Item itmChildDeptManagerMembers = inn.applyAML(aml);
		bool IsSetChildDeptManager=false;
		for(int k=0;k<itmChildDeptManagerMembers.getItemCount();k++)
		{		
			Item itmChildDeptManagerMember = itmChildDeptManagerMembers.getItemByIndex(k);
			if(itmChildDeptManagerMember.getPropertyItem("related_id").getProperty("is_alias","")!="1")
			{
				sql = "Update [Member] set related_id='" + strManagerRoleId + "' where id='" + itmChildDeptManagerMember.getID()  + "'";
				inn.applySQL(sql);
				IsSetChildDeptManager=true;
				break;
			}
		}
		if(IsSetChildDeptManager==false)
		{
			//代表子階部門主管可能根本沒有父階主管的關聯
			aml = "<AML>";
			aml += "<Item type='Member' action='add'>";
			aml += "<source_id>" + strChildManagerRoleId + "</source_id>";
			aml += "<related_id>" + strManagerRoleId + "</related_id>";
			aml += "</Item></AML>";
			inn.applyAML(aml);
		}
	}
	
	

	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
		strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateOrg' and [Method].is_current='1'">
<config_id>64BD086B6ECE42CCBE3EA1AB9F6091D1</config_id>
<name>In_UpdateOrg</name>
<comments>更新組織結構</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
