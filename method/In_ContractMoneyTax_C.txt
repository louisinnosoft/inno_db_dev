/*
目的:
做法:
1.檢查表單是否上鎖
位置:物件動作
*/

var inn = this.getInnovator();

//1.檢查表單是否上鎖
if(this.getLockStatus() !== 0)
{
	alert(inn.applyMethod("In_Translate","<text>請先解鎖本文件再執行</text>").getResult());
	return;
}

var itmApplyMethod = inn.applyMethod("In_MoneyTaxHandler","<parent_type>"+thisItem.getType()+"</parent_type><parent_id>"+thisItem.getID()+"</parent_id><rel_type></rel_type><money_tax>in_invoice_tax_o</money_tax><money>in_invoice_o</money><tax>in_tax_o</tax>");

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ContractMoneyTax_C' and [Method].is_current='1'">
<config_id>4E180CC32A2B47BAA5AE11DDD4285C57</config_id>
<name>In_ContractMoneyTax_C</name>
<comments>稅額計算(合約管理)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
