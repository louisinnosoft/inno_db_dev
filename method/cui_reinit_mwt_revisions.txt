if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame && workerFrame.itemTypeName === 'InBasket Task') {
		return {'cui_disabled': true};
	}

	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calculate_states', '', {eventType: inArgs.eventType});
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}

	if (workerFrame && workerFrame.grid) {
		var itemIDs = workerFrame.grid.getSelectedItemIds();
		var state = workerFrame.isVersionableIT && !inArgs.eventState.isTemp && itemIDs && itemIDs.length == 1;
		return {'cui_disabled': !state};
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_revisions' and [Method].is_current='1'">
<config_id>5F89703A22D54C5183CAFF0A9079F441</config_id>
<name>cui_reinit_mwt_revisions</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
