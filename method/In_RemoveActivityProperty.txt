/*目的:移除 日期欄位,避免格式錯誤
位置:onBeforeAdd
*/
this.removeProperty("in_enddate_sche");

this.removeProperty("in_enddate_act");
this.removeProperty("in_startdate_sche");
this.removeProperty("in_startdate_act");
this.removeProperty("in_ext4");
return this;
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RemoveActivityProperty' and [Method].is_current='1'">
<config_id>2A51877DD6754FC0B99E4650852218EE</config_id>
<name>In_RemoveActivityProperty</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
