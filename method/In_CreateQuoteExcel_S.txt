/*
目的:產生報價單PDF
做法:
1.產生queue
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.InnoDocument _InnDocu = new Innosoft.InnoDocument(inn);

int intOrder = 1;
int intTemplate = 1;

string aml = "";
string sql = "";
string param = "";
string strPos = "";

Item itmR = null;
	
try
{
    aml = "<AML>";
    aml += "<Item type='User' action='get'>";
    aml += "<id>"+this.getProperty("in_sales","")+"</id>";
    aml += "</Item></AML>";
    Item itmUser = inn.applyAML(aml);
    
    int intDec = Convert.ToInt32(this.getProperty("in_currency_decimal","0"));
	
	param  = "<in_param>";
	param += "<prefix>^</prefix>";
	param += "<sheet name='Quote'>";
	param += "<body_variable>";
	
	param += "<variable name='in_customer_name' datatype=''>" + this.getProperty("in_customer_name","") + "</variable>";
	
	param += "<variable name='in_context1' datatype=''>" + this.getProperty("in_context1","") + "</variable>";
	
	param += "<variable name='in_tel1' datatype=''>" + this.getProperty("in_tel1","");
	if(this.getProperty("in_tel2","")!="")
	{
	    param += "," + this.getProperty("in_tel2","");
	}
	param += "</variable>";
	
	param += "<variable name='in_fax1' datatype=''>" + this.getProperty("in_fax1","") + "</variable>";
	
	param += "<variable name='in_email1' datatype=''>" + this.getProperty("in_email1","") + "</variable>";
	param += "<variable name='in_customer_address' datatype=''>" + this.getProperty("in_customer_address","") + "</variable>";
	
	param += "<variable name='item_number' datatype=''>" + this.getProperty("item_number","") + "</variable>";
	
	param += "<variable name='in_sales_name' datatype=''>" + itmUser.getProperty("first_name","") + "</variable>";
	param += "<variable name='in_sales_cellphone' datatype=''>" + this.getProperty("in_sales_cellphone","") + "</variable>";
	
	param += "<variable name='in_sales_email' datatype=''>" + this.getProperty("in_sales_email","") + "</variable>";
	
	param += "<variable name='in_date' datatype=''>";
	if(this.getProperty("in_date","")!="")
	{	
		param += Convert.ToDateTime(this.getProperty("in_date","")).ToString("yyyy-MM-dd");
	}
	param += "</variable>";
	
	param += "<variable name='in_name' datatype=''>" + this.getProperty("in_name","") + "</variable>";

	param += "<variable name='in_fin_price_tax' datatype=''>";
	param += Convert.ToDouble(this.getProperty("in_fin_price_tax","0")).ToString("N"+ intDec.ToString());
	param += "</variable>";
	
	param += "<variable name='in_tax' datatype=''>";
	param += Convert.ToDouble(this.getProperty("in_tax","0")).ToString("N"+ intDec.ToString());
	param += "</variable>";
	
	param += "<variable name='in_ext_price' datatype=''>";
	param += Convert.ToDouble(this.getProperty("in_ext_price","0")).ToString("N"+ intDec.ToString());
	param += "</variable>";
	
	param += "<variable name='in_payment_condition_p_value' datatype=''>" + this.getProperty("in_payment_condition_p_value","")+  "</variable>";
	
	param += "<variable name='in_desc' datatype=''>" + this.getProperty("in_desc","")+  "</variable>";
	
	param += "</body_variable>";
	param += "<repeat_variable>";
	
	aml = "<AML>";
	aml += "<Item type='In_Quote_Details' action='get' >";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	Item itmQuoteDetails = inn.applyAML(aml);
	for(int i=0;i<itmQuoteDetails.getItemCount();i++)
	{
		Item itmQuoteDetail = itmQuoteDetails.getItemByIndex(i);
		
		if(itmQuoteDetail.getProperty("in_show_name","0")=="0")
		{
		    continue;
		}
		
		string strRowAttribute="";
		string strLevel = itmQuoteDetail.getProperty("in_level","0");
		
		if(strLevel=="1")
		{
		    strRowAttribute = " bgcolor='#BFBFBF'";
		}
		
		if(i==0)
		{
		    strPos = "fix";
		}else{
		    strPos = "continue";
		}
		
		param += "<row template='"+intTemplate.ToString()+"' order='"+intOrder.ToString() +"' pos='"+strPos+"'>";
		param += "<variable name='in_part_name' datatype=''>";
		if(strLevel=="2")
		{
		    param += "--";
		}
		if(strLevel=="3")
		{
		    param += "----";
		}
		param += itmQuoteDetail.getProperty("in_part_name","");
		param += "</variable>";
		
		param += "<variable name='in_qty' datatype=''>";
		if(itmQuoteDetail.getProperty("in_show_qty","0")=="1")
		{
		    param +=  itmQuoteDetail.getProperty("in_qty","");
		}
		param += "</variable>";
		
		param += "<variable name='in_unit_ext_price' datatype=''>";
		if(itmQuoteDetail.getProperty("in_show_qty","0")=="1")
		{
		    param += itmQuoteDetail.getProperty("in_unit_ext_price","0");
		}
		param += "</variable>";
		
		param += "<variable name='unit' datatype=''>";
		if(itmQuoteDetail.getProperty("in_show_qty","0")=="1")	
		{
		    param += _InnH.GetListLabel("Units",itmQuoteDetail.getProperty("in_part_unit","0"),"");
		}
		param += "</variable>";
		
		param += "<variable name='in_ext_price' datatype=''>";
		if(itmQuoteDetail.getProperty("in_show_price","0")=="1")	
		{
		    param += itmQuoteDetail.getProperty("in_ext_price","0");
		}
		param += "</variable>";
		
		param += "<variable name='in_memo' datatype=''>";
		param += itmQuoteDetail.getProperty("in_memo","");
		param += "</variable>";
		
		param += "</row>";
		
		intOrder ++;
	}
	
	param += "</repeat_variable>";
	param += "</sheet>";
	
	//從範本下載Excel
	param += "<source_file_aml>";
	param += "<Item type='In_Variable_Detail' action='get' select='in_file' maxRecords='1'>";
	param += "<source_id>";
	param += "<Item type='In_Variable' action='get' select='id'>";
	param += "<in_name>Template_In_Quote</in_name>";
	param += "</Item>";
	param += "</source_id>";
	param += "</Item>";
	param += "</source_file_aml>";
	param += "<source_property>in_file</source_property>";
	
	param += "<return_itemtype>" + this.getType() + "</return_itemtype>";
	param += "<return_id>" + this.getID() + "</return_id>";
	
	param += "<return_filename>" + this.getProperty("item_number","") + "-" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm") + ".xlsx</return_filename>";
            
        param += "<return_item type='aml'>";
        param += "<Item type='In_Quote_sysreport' action='add'>";
        param += "<source_id>" + this.getID() + "</source_id>";
        param += "<related_id>@fileid</related_id>";
        param += "</Item></return_item>";
        
        param += "</in_param>";
        
        Item itmQueue = _InnH.CreateQueue(this,"xml_to_excel",param,"");
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateQuoteExcel_S' and [Method].is_current='1'">
<config_id>7EBDE36F80C547E6AB4E6173BC1B87B6</config_id>
<name>In_CreateQuoteExcel_S</name>
<comments>產生報價單PDF</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
