Item fullItem = this.newItem(this.getType(), "get");
fullItem.setID(this.getID());
fullItem = fullItem.apply();

this.setPropertyItem("related_id", fullItem.getPropertyItem("related_id"));


return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_populate_related_id_item' and [Method].is_current='1'">
<config_id>4A318D77BC3B42F68925F835ADE55FD4</config_id>
<name>PE_populate_related_id_item</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
