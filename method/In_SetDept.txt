/*
目的:將部門(in_dept)設定為建立者部門
作法:
1.取得登入者
2.更新部門欄位
*/

//1.取得登入者
var aliasId = this.getInnovator().getUserAliases();
var alias = this.getInnovator().getItemById("Identity", aliasId);

//2.更新部門欄位
this.setProperty("in_dept",alias.getProperty("in_dept"));

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetDept' and [Method].is_current='1'">
<config_id>B81733F9802E48E6ACE601598039FA28</config_id>
<name>In_SetDept</name>
<comments>將部門(in_dept)設定為建立者部門</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
