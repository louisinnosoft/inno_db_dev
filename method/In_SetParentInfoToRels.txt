/*
目的:將表頭的creator,owner, manager, team_id 強制押到 關聯線上
做法:
1.取得這個Itemtype的所有relationship
2.使用sql update 更新全部的線
*/
string strMethodName = "In_SetParentInfoToRels";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strDebugInfo = "";

Item itmR = this;
Item itmProject = null;
string strProject = "";
	
try
{
    //1.取得這個Itemtype的所有relationship,且是use src access 的
	sql = "select name from RelationshipType where source_id='" + this.getAttribute("typeId") + "'";
	Item itmRels = inn.applySQL(sql);
	if(itmRels.getResult()!="")
	{
		for(int i=0;i<itmRels.getItemCount();i++)
		{
			Item itmRel = itmRels.getItemByIndex(i);
			sql = "Update [" + itmRel.getProperty("name").Replace(" ","_") + "] set ";
			sql += " owned_by_id=";
			if(this.getProperty("owned_by_id","")=="")
				sql+= "null";
			else
				sql += "'" + this.getProperty("owned_by_id","") + "'";

			sql += " ,managed_by_id=";
			if(this.getProperty("managed_by_id","")=="")
				sql+= "null";
			else
				sql += "'" + this.getProperty("managed_by_id","") + "'";

			sql += " ,team_id=";
			if(this.getProperty("team_id","")=="")
				sql+= "null";
			else
				sql += "'" + this.getProperty("team_id","") + "'";

			sql += " ,created_by_id=";
			if(this.getProperty("created_by_id","")=="")
				sql+= "null";
			else
				sql += "'" + this.getProperty("created_by_id","") + "'";

			sql += " where source_id='" + this.getID() + "'";
			
			inn.applySQL(sql);
		}

	}
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	strError += strDebugInfo + "\n";
	
	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetParentInfoToRels' and [Method].is_current='1'">
<config_id>5B4AB11CFCF041068CF9237A29D82F97</config_id>
<name>In_SetParentInfoToRels</name>
<comments>將表頭的creator,owner, manager, team_id 強制押到 關聯線上</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
