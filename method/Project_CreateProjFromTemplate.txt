var errorMsg = "";

if (top.aras.isTempEx(this.node) || top.aras.isDirtyEx(this.node) || top.aras.isNew(this.node)) {
	errorMsg = top.aras.getResource("Project", "project.cannot_be_cloned", "Project Template");
	top.aras.AlertError(errorMsg);
	return;
}

var clone = this.newItem();
clone.loadAML("<AML>" + this.node.xml + "</AML>");
clone.setAttribute("projectCloneMode", "CreateProjectFromTemplate");
var newProject = clone.apply("Project_CloneProjectOrTemplate");
if (newProject.isError()) {
	 top.aras.AlertError(newProject.getErrorString(), newProject.getErrorDetail());
}
top.aras.uiShowItemEx(newProject.node);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_CreateProjFromTemplate' and [Method].is_current='1'">
<config_id>4B4C8F86C2354DFBB9AAD18B454CA404</config_id>
<name>Project_CreateProjFromTemplate</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
