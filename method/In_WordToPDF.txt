//System.Diagnostics.Debugger.Break();
//目的:創建一個藥證文件的Word to PDF 轉檔排程
//傳入格式:<drug_lic_id>drug_lic_id</drug_lic_id><document_id>document_id</document_id><source_itemtype>drug_lic</source_itemtype><source_id>drug_lic_deliverable_id</source_id>

Innovator inn = this.getInnovator();
//創建 In_Queue 的 AML
string strCreateQueue = "";
strCreateQueue += "<AML>";
strCreateQueue += "<Item type='In_Queue' action='add'>";
strCreateQueue += "<in_source_itemtype>" + this.getProperty("source_itemtype","") + "</in_source_itemtype>";
strCreateQueue += "<in_source_id>" + this.getProperty("source_id","") + "</in_source_id>";
strCreateQueue += "<in_source_keyed_name>@keyed_name</in_source_keyed_name>";
strCreateQueue += "<in_type>drug_word_to_pdf</in_type>";
strCreateQueue += "<in_state>start</in_state>";
strCreateQueue += "<in_inparam><![CDATA[@in_inparam]]></in_inparam>";
strCreateQueue += "<in_state_method>";
strCreateQueue += "<Item type='Method' action='get' select='id'>";
strCreateQueue += "<name>In_Queue_DLDel</name>";
strCreateQueue += "</Item>";
strCreateQueue += "</in_state_method>";
strCreateQueue += "</Item></AML>";

string in_inparam="";

//取得藥證  Item
string aml = "";
aml = "<AML>";
aml += "<Item type='In_Drug_Lic' action='get' select='keyed_name,in_project_number,in_product_name'>";
aml += "<id>" + this.getProperty("drug_lic_id","") + "</id>";
aml += "</Item>";
aml += "</AML>";
Item itmDrugLic = inn.applyAML(aml);

//取得藥證文件  Item
aml = "<AML>";
aml += "<Item type='Document' action='get' select='config_id,in_native_file(file_type(extension)),keyed_name,in_approval_chapter(in_code,in_name_tw,in_name_en)'>";
aml += "<id>" + this.getProperty("document_id","") + "</id>";
aml += "</Item>";
aml += "</AML>";
Item itmDocument = inn.applyAML(aml);

in_inparam = "<in_param>"; 

//轉檔資訊區塊
in_inparam += "<source_itemtype>Document</source_itemtype>";
in_inparam += "<source_id>" + itmDocument.getID() + "</source_id>";
//放config_id 是因為可能在轉檔的時候,source_item已經改版了,因此要抓最新版改版
in_inparam += "<source_config_id>" + itmDocument.getProperty("config_id","") + "</source_config_id>";
in_inparam += "<source_property>in_word_file</source_property>";

//完成檔案回填資訊區塊
in_inparam += "<return_itemtype>Document</return_itemtype>";
in_inparam += "<return_id>" + itmDocument.getID() + "</return_id>";
in_inparam += "<return_config_id>" + itmDocument.getProperty("config_id","") + "</return_config_id>";
in_inparam += "<return_property>in_modify_file</return_property>";

//其他資訊


in_inparam += "</in_param>";

string strKeyed_name = "";
strKeyed_name = itmDrugLic.getProperty("keyed_name","") + "-" + itmDocument.getProperty("keyed_name","");

strCreateQueue = strCreateQueue.Replace("@in_inparam",in_inparam);
strCreateQueue = strCreateQueue.Replace("@keyed_name",strKeyed_name);


Item In_Queue = inn.applyAML(strCreateQueue);
if(In_Queue.isError())
	throw new Exception(In_Queue.getErrorString());
return this;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_WordToPDF' and [Method].is_current='1'">
<config_id>348DD938658547F580D123B10EF12827</config_id>
<name>In_WordToPDF</name>
<comments>drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
