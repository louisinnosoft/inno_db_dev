var filter = {};
filter['source_id'] = {
	filterValue: aras.getItemProperty(item, 'id'),
	isFilterFixed: true
};
return filter;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ElementTypeFilter' and [Method].is_current='1'">
<config_id>6353537F68F24D6C89E6C8303E35E63F</config_id>
<name>cmf_ElementTypeFilter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
