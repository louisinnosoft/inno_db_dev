//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Timesheet Manager");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

string aml = "";
string strResponse = "";

string strQueueID = this.getProperty("queue_id","");

if(strQueueID!=""){
    
    aml = "<AML>";
    aml += "<Item type='In_Queue' action='get' id='" + strQueueID + "'>";
    aml += "</Item></AML>";
    Item itmQueue = inn.applyAML(aml);
    
    string strParam = itmQueue.getProperty("in_inparam");
    Item itmParam = inn.newItem("param");
    itmParam.loadAML(strParam);
    
    string strIDs = itmParam.getProperty("ids");
    string strWorkDate = itmParam.getProperty("workdate")+"T00:00:00";
    strResponse = itmParam.getProperty("response");
    
    _InnH.ChangeQueueState(itmQueue, "In_Process");
    
    aml = "<AML>";
    aml += "<Item type='Identity' action='get'>";
    aml += "<id condition='in'>"+strIDs+"</id>";
    aml += "</Item></AML>";
    Item itmIdentities = inn.applyAML(aml);
    
    for(int i=0;i<itmIdentities.getItemCount();i++){
            Item itmIdentity = itmIdentities.getItemByIndex(i);
            
            aml = "<AML>";
            aml += "<Item type='User' action='get'>";
            aml += "<id>"+itmIdentity.getProperty("in_user","")+"</id>";
            aml += "</Item></AML>";
            Item itmUser = inn.applyAML(aml);
            
            //建立空白的工時紀錄表(前一工作日)
            aml = "<AML>";
		    aml += "<Item type='In_TimeRecord' action='add'>";
	        aml += "<in_company>"+itmIdentity.getProperty("in_company","")+"</in_company>";
	        aml += "<in_project>";
	        aml += "<Item type='Project' action='get'>";
	        aml += "<in_number>99999</in_number>";	
	        aml += "</Item>";
	        aml += "</in_project>";
	        aml += "<in_start_time>"+strWorkDate+"</in_start_time>";
	        aml += "<in_today_hours>8</in_today_hours>";
       	    aml += "<owned_by_id>"+itmIdentity.getID()+"</owned_by_id>";
    	    aml += "<in_function_code>";
	        aml += "<Item type='In_Function_Code' action='get'>";
	        aml += "<in_code>L05</in_code>";
	        aml += "</Item>";
	        aml += "</in_function_code>";
	        aml += "<description>國定假</description>";
	        aml += "<in_work_hours>"+itmUser.getProperty("in_day_hour","")+"</in_work_hours>";
	        aml += "</Item></AML>";
    	    Item itmTimeRecord = inn.applyAML(aml);
    	    itmTimeRecord.promote("Approved","");
    	    
    	    string strMsgAdmin = "員工：" + itmIdentity.getProperty("keyed_name","") + "未填寫 " + strWorkDate + " 工時紀錄表";
    	    Innosoft.InnUtility.AddLog(strMsgAdmin,"未報工紀錄");
    }
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreatePayHolidayTimeRecord' and [Method].is_current='1'">
<config_id>824E4CE870614842812CB0BA6E775F29</config_id>
<name>In_CreatePayHolidayTimeRecord</name>
<comments>給薪假工時卡</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
