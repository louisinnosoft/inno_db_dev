/*version: 5*/
var _form = document.forms.MainDataForm;
var _isVers = _form.elements['is_versionable'].checked;
var versioningDisciplineFld = _form.elements['versioning_discipline'];

var item = document.item;
if (window.handleItemChange) {
	if (_isVers) {
		var isRelationship = (aras.getItemProperty(item, 'is_relationship') == '1');
		var versioningDiscipline = isRelationship ? 'BaselineVersioningDiscipline' : '0';
		handleItemChange('manual_versioning', versioningDiscipline);
	} else {
		handleItemChange('manual_versioning', '');
		aras.setItemPropertyAttribute(item, 'manual_versioning', 'is_null', '1');
	}
}
if (versioningDisciplineFld) {
	versioningDisciplineFld.disabled = (!_isVers);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='is_versionable onClick handler' and [Method].is_current='1'">
<config_id>A48720B96EB64147A392060806967796</config_id>
<name>is_versionable onClick handler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
