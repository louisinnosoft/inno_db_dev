/*
目的:請款單單身請款金額累計
做法:使用 SumRelationshipPropertyToSourceProperty
*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
	
try
{
	//SumRelationshipPropertyToSourceProperty(Item SourceItem, string RelationshipName, string RelationshipPropertyName, string SourcePropertyName)
	_InnH.SumRelationshipPropertyToSourceProperty(this,"In_Vendor_Payment_Detail","in_money_tax","in_deatil_amount");
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml  + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Total_Amount' and [Method].is_current='1'">
<config_id>538A3E491CA344A5BCC03B6FC0B84078</config_id>
<name>In_Total_Amount</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
