string userId = this.getID();
if (String.IsNullOrEmpty(userId))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_FailedDefineUserIdNotSet"));
}
List<string> userIdentities = GetAllUserIdentities(userId);
if (userIdentities.Count < 1)
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_FailedDefineUserNoIdentites"));
}
context = CCO;
return this.getInnovator().newResult(IsUserAdmin(userIdentities).ToString());
}
Aras.Server.Core.CallContext context;
private List<string> GetAllUserIdentities(string userId)
{
	Item identitiesIds = this.newItem("user", "VC_GetAllUserIdentities");
	identitiesIds.setID(userId);
	identitiesIds = identitiesIds.apply();
	return identitiesIds.getResult().Split('|').ToList();
}
private bool IsUserAdmin(List<string> userIdentities)
{
	const string adminIdentityId = "2618D6F5A90949BAA7E920D1B04C7EE1";
	Aras.Server.Core.InnovatorDatabase database = context.Variables.InnDatabase;
	List<string> identityMembers = Aras.Server.Security.Permissions.GetAncestorIdentityIds(database, new string[]{ adminIdentityId}).ToList();
	return identityMembers.Any(x=> userIdentities.Any(y=> x==y));
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_IsUserAdmin' and [Method].is_current='1'">
<config_id>AFA06E6F9A74455FA352850B02001BCA</config_id>
<name>VC_IsUserAdmin</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
