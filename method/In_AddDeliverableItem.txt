/*
In_AddDeliverableItem
目的:在Vote頁面中回報流程要繳付的資料
可依據Itemtype或deliverabletype決定要將資料掛到哪一個relationship上
傳入參數:
<itemtype>:要被掛載的物件本體TypeName (例如 In_Workorder)
<itemid>:要被掛載的物件本體TypeId(例如 In_Workorder)
<deliverabletype>:繳付物TypeName(例如 Document)
<deliverableid>:繳付物(例如 Document)
做法:
1.依據itemtype與deliverabletype找到正確的relationship
2.新增一個relationship
*/

/*
目前僅鎖定 In_WorkOrder
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

Item itmR = inn.newItem("R");
string str_r="";
	
try
{
	//string strRelationships = "In_WorkOrder-Document,In_WorkOrder_Activity";
	string strItemType = this.getProperty("itemtype");
	string strItemId = this.getProperty("itemid");
	string strDeliverableType = this.getProperty("deliverabletype");
	string strDeliverableId = this.getProperty("deliverableid");

	string strCurrentUserIdentityId =  _InnH.GetIdentityByUserId(inn.getUserID()).getID();
	sql = "select * from [In_WorkOrder_Detail] where";
	sql += " source_id='" + strItemId + "'";
	sql += " and in_is_current='1'";
	sql += " and in_new_assignment in (" + Aras.Server.Security.Permissions.Current.IdentitiesList + ")";			
	Item itmWorkorderDetails = inn.applySQL(sql);
	Item itmWorkorderDetail=null;
	string in_workorder_detail="";
	if(itmWorkorderDetails.getResult()!="")
	{
		itmWorkorderDetail = itmWorkorderDetails.getItemByIndex(0);
		in_workorder_detail = itmWorkorderDetail.getProperty("in_name","");
	}

	Item itmTmp=null;
	
	aml = "<AML>";
	aml += "<Item type='In_WorkOrder_Deliverable' action='add'>";
	aml += "<source_id>" + strItemId + "</source_id>";
	aml += "<related_id>" + strDeliverableId + "</related_id>";
	aml += "<in_workorder_detail>" + in_workorder_detail + "</in_workorder_detail>";
	aml += "</Item></AML>";
	itmTmp = inn.applyAML(aml);
	
	//也一併帶到Activity2
	if(itmWorkorderDetail!=null && itmWorkorderDetail.getProperty("related_id","")!="")
	{
		aml = "<AML>";
		aml += "<Item type='Activity2 Deliverable' action='add'>";
		aml += "<source_id>" + itmWorkorderDetail.getProperty("related_id") + "</source_id>";
		aml += "<related_id>" + strDeliverableId + "</related_id>";
		aml += "</Item></AML>";
		itmTmp = inn.applyAML(aml);				
	}
	str_r = "繳付文件成功";
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	//throw new Exception(_InnH.Translate(strError));
	str_r = "繳付文件失敗:" + strError;
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return inn.newResult(_InnH.Translate(str_r));

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddDeliverableItem' and [Method].is_current='1'">
<config_id>FAADB5EB65CE4A08B45C1FB9EFFFE8D5</config_id>
<name>In_AddDeliverableItem</name>
<comments>在Vote頁面中回報流程要繳付的資料,可交付文件與檔案</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
