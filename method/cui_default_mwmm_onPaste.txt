var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onPasteCommand) {
	workerFrame.onPasteCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onPaste' and [Method].is_current='1'">
<config_id>B0F8797BB99D47E391EF069F0E90D352</config_id>
<name>cui_default_mwmm_onPaste</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
