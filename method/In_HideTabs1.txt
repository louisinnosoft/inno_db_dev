document.In_HideTabs = function(TabNames)
{
  if (!parent.relationships)
  {
    
    setTimeout("document.In_HideTabs('" + TabNames + "');", 100);
    return; 
  }
   if (parent.relationships.document.readyState != "complete" || !parent.relationships.currTabID)
   {
    
    setTimeout("document.In_HideTabs('" + TabNames + "');", 100);
   }
  else
  {
   
   
	var arrTabNames=TabNames.split(",");  
    var tabbar = parent.relationships.relTabbar;
	for ( var i= 0;i <arrTabNames.length;i ++)
	{
		var tabID =  top.aras.getRelationshipTypeId(arrTabNames[i]);
		tabbar.SetTabVisible(tabID,false);
	}
	  
  }
} ;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_HideTabs1' and [Method].is_current='1'">
<config_id>4431468D008B4B6193947CD27604CB9C</config_id>
<name>In_HideTabs1</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
