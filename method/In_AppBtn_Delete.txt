/*
目的:APPAction:刪除按鈕
用法:item.apply
須回傳:
app action
(若回傳 null 則代表不顯示按鈕)

位置:App Action的method
*/
string strMethodName = "In_AppBtn_Delete";
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.app _InnoApp = new Innosoft.app(inn);
string aml = "";
string sql = "";
string strError = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmAction = null;
//只有檢視模式才需要處理
if(this.getProperty("sort","")=="in_web_add" || this.getProperty("sort","")=="in_web_edit")
	return  itmAction;

if(this.fetchLockStatus()==2)
	return itmAction;

string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;
bool CanDelete = _InnoApp.CanAccess(identity_list,this,"can_delete");
if(!CanDelete)
	return itmAction;

if(this.fetchLockStatus()==1)
{
	//如果是被自己鎖定則解鎖	
	try
	{
		this.unlockItem();
	}
	catch (Exception ex)
	{
		return  itmAction;
	}
}

itmAction = inn.newItem();
itmAction.setType("action");
itmAction.setProperty("href","");
itmAction.setProperty("btn_type","report");		
string strClick= "ih.DeleteItem('" + this.getType() + "','" + this.getID()+"')";
itmAction.setProperty("click",strClick);
itmAction.setProperty("label","刪除");
itmAction.setProperty("class","");
itmAction.setProperty("data_toggle","");
itmAction.setProperty("data_target","");
	

return itmAction;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppBtn_Delete' and [Method].is_current='1'">
<config_id>C12C97E9811C4579BF3D2DF0C8F6001F</config_id>
<name>In_AppBtn_Delete</name>
<comments>APPAction:刪除按鈕</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
