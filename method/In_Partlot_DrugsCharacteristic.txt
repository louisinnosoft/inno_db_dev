Innovator inn = this.getInnovator();
//System.Diagnostics.Debugger.Break();
string Parameter = "";

//先判斷本身的In_Drug_Drug_testfocus是否有值,如果已經有Relationship, 則忽略不做
Item itmDrugs_Drugs_Characteristic = inn.newItem("In_Drug_Drug_testfocus","get");
itmDrugs_Drugs_Characteristic.setProperty("source_id",this.getID());
itmDrugs_Drugs_Characteristic = itmDrugs_Drugs_Characteristic.apply();
if(itmDrugs_Drugs_Characteristic.getItemCount()>0)
	return this;


Item itmDrugs_Part = inn.newItem("Part","get");
itmDrugs_Part.setProperty("source_id",this.getID());
itmDrugs_Part = itmDrugs_Part.apply();

//for(int i=0;i<itmDrugs_Part.getItemCount();i++)
//{
	var in_relpart_id = this.getProperty("in_relpart","");
	if(in_relpart_id!=""){
		Item itm = inn.getItemById("Part",in_relpart_id);
		if(!itm.isError()){
			Parameter="<FromRelName>Part Drugs Characteristic</FromRelName>";
			Parameter += "<ToRelName>In_Drug_Drug_testfocus</ToRelName>";
			Parameter += "<FromSourceID>" + itm.getProperty("id","") + "</FromSourceID>";
			Parameter += "<ToSourceID>" + this.getID() + "</ToSourceID>";
			Parameter += "<FromSourceName>Part</FromSourceName>";
			Parameter += "<ToSourceName>Drugs</ToSourceName>";
		//	Parameter += "<CopyProperies>in_fda_date,in_fda_standard,in_gmp_standard</CopyProperies>";
			Parameter += "<CopyProperies>related_id,in_target,in_fda_date,in_gmp_date,in_fda_standard,in_gmp_standard,in_gcp,in_glp,in_decision_condition,in_determination_results</CopyProperies>";
			
			Item r = inn.applyMethod("in_CopyRelationships",Parameter);
		}
	}
//}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Partlot_DrugsCharacteristic' and [Method].is_current='1'">
<config_id>2579895A5985488EB57416D64F5EE56C</config_id>
<name>In_Partlot_DrugsCharacteristic</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
