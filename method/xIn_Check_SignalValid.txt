/*
    檢查所屬物件是否帶有 inn_signal屬性並且其內部是否含有signal參數。
    本方法主要提供客製事件時檢查是否應該執行該方法。
    不須傳入任何參數。
    回傳： 
        0 : 不符合
        1 : 符合
    
*/

Item itmParam=this.getPropertyItem("inn_signal");
if(itmParam==null){
    return inn.newResult("0");
}

string strParam=itmParam.getProperty("signal","no_data");
if(signal=="no_data"){
    return inn.newResult("0");
}

return inn.newResult("1");


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xIn_Check_SignalValid' and [Method].is_current='1'">
<config_id>DBB1F798721447AF8F67997452E54CE1</config_id>
<name>xIn_Check_SignalValid</name>
<comments>檢查是否攜帶有inn_signal屬性值，且signal屬性有值。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
