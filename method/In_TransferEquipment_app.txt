/*
目的:變更設備的Owner
做法:
1.剖析 Criteria 的 id
2.將目前的 User 更新為 設備的Owner
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
 Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
 Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
 
string strId = Innosoft.InnUtility.GetParameterValue(criteria,"id",'&',':',"");

string strLoginName = _InnoApp.GetUserInfo(UserInfo, "loginid");
Item itmLoginIdentity = _InnH.GetIdentityByUserLoginName(strLoginName);

string aml = "";
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	aml = "<AML>";
	aml += "<Item type='In_Equipment' id='" + strId + "' action='edit'>";
	aml += "<owned_by_id>" + itmLoginIdentity.getID() + "</owned_by_id>";
	aml += "</Item></AML>";
	Item itm_R = inn.applyAML(aml);
	
	if(itm_R.isError())
		throw new Exception(itm_R.getErrorString());
	string strWoID = itmLoginIdentity.getID();
DateTime dtNow = DateTime.Now;
string strNow = dtNow.ToString("yyyy-MM-ddTHH:mm:ss");

aml = "<AML>";
aml += "<Item type='In_Equipment_History' action='edit' where=\"source_id='" +  strId + "' and isnull(in_end_date,'')=''\" >";
aml += "<in_end_date>" +  strNow + "</in_end_date>";
aml += "<in_movement>3</in_movement>";
aml += "</Item></AML>";
	Item itmEQPs = inn.applyAML(aml);

aml = "<AML>";
	aml += "<Item type='In_Equipment_History' action='add'>";
	aml += "<source_id>" + strId + "</source_id>";
	aml += "<in_start_date>" +  strNow + "</in_start_date>";
	aml += "<in_owner>" +  strWoID + "</in_owner>";
	aml += "<in_movement>1</in_movement>";
	aml += "</Item></AML>";
	itmEQPs = inn.applyAML(aml);
	r = _InnoApp.BuildResponse("true","ok","",criteria);
}
catch (Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TransferEquipment_app' and [Method].is_current='1'">
<config_id>B29D60AA87CD45388F1E2394809FD820</config_id>
<name>In_TransferEquipment_app</name>
<comments>inn app</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
