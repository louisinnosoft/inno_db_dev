//  can be used on any Item Type with the 
// "OnAfterNew" Event (Client Events tab)
// will set the owned_by_id to the current user's id (creator)
//debugger;
if ("" === this.getProperty("owned_by_id",""))
{
  var aliasId = this.getInnovator().getUserAliases();
  if (aliasId)
  {
    aliasId = aliasId.substr(0,32);
    var alias = this.getInnovator().getItemById("Identity", aliasId);
    if (alias.getItemCount()==1)
    {
      this.setProperty("owned_by_id", alias.getAttribute("id"));
      this.setPropertyAttribute("owned_by_id", "keyed_name", alias.getProperty("keyed_name", ""));      
     
    }
  }
}


		//debugger;
		var now = new Date();
		var dd = now.getDate();
		var mm = now.getMonth()+1; //January is 0!

		var yyyy = now.getFullYear();
		if(dd<10){
			dd='0'+dd;
		} 
		if(mm<10){
			mm='0'+mm;
		} 
		var today = yyyy+'-'+mm+'-'+dd + "T00:00:00";

this.setProperty("in_r_date_from", today);
this.setProperty("in_r_date_to", today);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xIn_InitTimeRecord' and [Method].is_current='1'">
<config_id>572F39461775429C8F50AAE91B60A98F</config_id>
<name>xIn_InitTimeRecord</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
