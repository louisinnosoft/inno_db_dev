//in_RecordsToDrug_Approval
//將本筆申請記錄複製到多個藥品許可單之下

var param = {aras:top.aras,itemtypeName:'In_Drug_Lic',multiselect:1};
var res = showModalDialog('../scripts/searchDialog.html', param, 'dialogHeight:450px; dialogWidth:700px; status:0; help:0; resizable:1');
if (res === undefined || res.itemID ==='') {return false;}
var inn = this.getInnovator();
var param = "<In_Drug_Lics>" + res + "</In_Drug_Lics>";
param += "<FromDrug_Approval_RecordsID>" + this.getID() + "</FromDrug_Approval_RecordsID>";
var itm = inn.applyMethod("In_AddDrug_Lic_Records",param);
alert(this.getInnovator().applyMethod("In_Translate","<text>已新增" + itm.getItemCount() + "筆申請記錄</text>").getResult());
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RecordsToDrug_Approval' and [Method].is_current='1'">
<config_id>255A82A10BD14892B63FADFCB88BA74F</config_id>
<name>In_RecordsToDrug_Approval</name>
<comments>Inn Drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
