//傳入參數：
//  meeting_id:會議的id
//  surveytype:問卷種類
//  muid:會議使用者id
//  如果請求的問卷在有效期間內，回傳1
//  如果請求的問卷不再有效期間內，回傳2
//  如果請求的問卷使用者已填寫過，回傳3
string sheetPrefix="sheet";

Innovator inn=this.getInnovator();

//string meeting_id=this.getProperty("meeting_id","6908863025B74A008F8A8C1A88E2FBBF");
//string surveytype=this.getProperty("surveytype","2");
//string muid=this.getProperty("muid","90FD09CDAA90423B9097CC9493B7E210");
string meeting_id=this.getProperty("meeting_id","");
string surveytype=this.getProperty("surveytype","");
string muid=this.getProperty("muid","");
DateTime start;
DateTime end;

//檢查使用者是否已填答
Item answers=inn.newItem();
answers.setAttribute("type","In_Meeting_Surveys_result");
answers.setAttribute("action","get");
answers.setProperty("source_id",meeting_id);
answers.setProperty("in_surveytype",surveytype);
answers.setProperty("in_participant",muid);
answers=answers.apply();
//System.Diagnostics.Debugger.Break();


//若使用者以填寫過問卷，傳回3並結束處理。
if(!answers.isEmpty()){
    return inn.newResult("3");
}
Item functionTime=inn.newItem();
functionTime.setAttribute("type","In_Meeting_Functiontime");
functionTime.setAttribute("action","get");
functionTime.setProperty("source_id",meeting_id);
functionTime.setProperty("in_action",sheetPrefix+surveytype);
functionTime=functionTime.apply();

//如果找不到對應的In_Meeting_Functiontime時回傳4，基本上不會發生，因為在取得的過程中已經檢查過。但還是擺著以策安全。
if (functionTime.isEmpty()) {
    return inn.newResult("4");
    }

start=System.DateTime.Parse(functionTime.getProperty("in_date_s"));
end=System.DateTime.Parse(functionTime.getProperty("in_date_e"));

//如果在期間內，就傳回1，不在傳回2
if(System.DateTime.Now>=start && System.DateTime.Now<=end){
    return inn.newResult("1");
}else{
    return inn.newResult("2");
}




#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Check_MeetingSurveyActive' and [Method].is_current='1'">
<config_id>FE70CF1A227949269D975FBAECA23BF6</config_id>
<name>In_Check_MeetingSurveyActive</name>
<comments>檢查要求的survey是否在有效前間，及是否已填答。由InnoJSON.ashx呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
