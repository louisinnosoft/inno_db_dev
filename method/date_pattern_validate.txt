var field = grid['columns_Experimental'].get(colNumber, 'name');
if (getRelationshipProperty(relationshipID, 'data_type') != 'date') {
	grid['columns_Experimental'].set(field, 'editType', undefined);
	return true;
}

var listNm = 'DateFormats';
var listNd = aras.getItem('List', 'name="' + listNm + '"', '<name>' + listNm + '</name>', 1);
if (!listNd) {
	return true;
}

var labs = [];
var vals = [];
var rels = listNd.selectNodes('Relationships/Item[@type=\'Value\']');
for (var i = 0; i < rels.length; i++) {
	vals.push(aras.getItemProperty(rels[i], 'value'));
	labs.push(aras.getItemProperty(rels[i], 'label'));
}

var v = grid['items_Experimental'].get(relationshipID, 'value', field);
if (v && vals.indexOf(v) === -1) {
	vals.push(v);
	labs.push(v);
}

if (vals.indexOf('') === -1) {
	vals.unshift('');
	labs.unshift('');
}

grid['columns_Experimental'].set(field, 'editType', 'FilterComboBox');
grid['columns_Experimental'].set(field, 'comboList', vals, labs);
return true;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='date_pattern_validate' and [Method].is_current='1'">
<config_id>C01483311E4749FE877A64801BD52370</config_id>
<name>date_pattern_validate</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
