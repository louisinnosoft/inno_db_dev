/*
目的:立案單儲存時,將PM謄寫到專案的PM與Team
做法:
位置:
*/

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string strMethodName="In_SetPMToProject";
string aml = "";
string sql = "";
string strError="";
try{
    
    Item ControlledItem = _InnH.GetInnoControlledItem(this);
    
    if(ControlledItem.isError()){
        return inn.newError(ControlledItem.getErrorString());
    }
    
    aml = "<AML>";
    aml += "<Item type='Project' action='get'>";
    aml += "<in_number>"+ControlledItem.getProperty("item_number","")+"</in_number>";
    aml += "</Item></AML>";
    Item itmProject = inn.applyAML(aml);
	if(itmProject.isError())
	{
		throw new Exception("查無編號為:[" + ControlledItem.getProperty("item_number","") + "]的專案");
	}
	
	if(itmProject.getItemCount()>1)
	{
		throw new Exception("系統內有兩筆編號為:[" + ControlledItem.getProperty("item_number","") + "]的專案");
	}
    
    aml = "<AML>";
    aml += "<Item type='Team' action='get'>";
    aml += "<id>"+itmProject.getProperty("team_id","")+"</id>";
    aml += "</Item></AML>";
    Item itmTeam = inn.applyAML(aml);
	if(itmTeam.isError())
	{
		throw new Exception("編號為:[" + ControlledItem.getProperty("item_number","") + "]的專案無專案團隊(Team)物件");
	}
    
    aml = "<AML>";
    aml += "<Item type='Identity' action='get'>";
    aml += "<name>Team Manager</name>";
    aml += "</Item></AML>";
    Item itmIdentity = inn.applyAML(aml);
    
	
	aml = "<AML>";
	aml += "<Item type='Team Identity' action='delete' where=\"[team_role]='" + itmIdentity.getID() + "'\">";
	aml += "<source_id>"+itmTeam.getID()+"</source_id>";
	aml += "</Item></AML>";
	Item itmTeamIdentityDel = inn.applyAML(aml);
	
    
    
    sql = "Update Project set ";
    sql += "owned_by_id = '" + ControlledItem.getProperty("in_pm","") + "'";
    sql += ",in_dept = '" + ControlledItem.getProperty("in_dept","") + "'";
    sql += " where id = '" + itmProject.getID() + "'";
    inn.applySQL(sql);
    
    sql = "Update Team set ";
    sql += "owned_by_id = '" + ControlledItem.getProperty("in_pm","") + "'";
    sql += " where id = '" + itmProject.getProperty("team_id","") + "'";
    inn.applySQL(sql);
    
	
	aml = "<AML>";
	aml += "<Item type='Team Identity' action='add' where=\"[team_role]='" + itmIdentity.getID() + "'\">";
	aml += "<source_id>"+itmTeam.getID()+"</source_id>";
	aml += "<related_id>"+ControlledItem.getProperty("in_pm")+"</related_id>";
	aml += "<team_role>"+itmIdentity.getID()+"</team_role>";
	aml += "</Item></AML>";
	Item itmTeamIdentityAdd = inn.applyAML(aml);
	
    
}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string errMsg = "";
    if(ex.GetType().ToString()=="Aras.Server.Core.InnovatorServerException")
    {
        Aras.Server.Core.InnovatorServerException e = (Aras.Server.Core.InnovatorServerException)ex;
        XmlDocument errDom = new XmlDocument();
        e.ToSoapFault(errDom); 
        errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerText;
        if(errMsg=="")
            errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerXml;
    }
    else
    {
        errMsg=ex.Message;
    }
	
	if(strError=="")
	{
		strError = errMsg + "\n";

		if(ex.Source=="IOM")
		{
			//代表這是非預期的錯誤
			if(aml!="")
				strError += "無法執行AML:" + aml  + "\n";

			if(sql!="")
				strError += "無法執行SQL:" + sql  + "\n";
		}
	}	
	string strErrorDetail="";
	strErrorDetail = strMethodName + ":" + strError; 
	_InnH.AddLog(strErrorDetail,"Error");
	//strError = strError.Replace("\n","</br>");
	
	throw new Exception(_InnH.Translate(strError));
}
if(PermissionWasSet)
Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetPMToProject' and [Method].is_current='1'">
<config_id>8E866B147E4F43A487C2C90A96BE520B</config_id>
<name>In_SetPMToProject</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
