var warningText = aras.getResource('../Modules/aras.innovator.CMF/', 'cmf_automigration_dataloss_warning');
return aras.confirm(warningText);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_PropertyTypeDeleteClient' and [Method].is_current='1'">
<config_id>9694F89C1FEF46248D2CFD84A6D9E336</config_id>
<name>cmf_PropertyTypeDeleteClient</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
