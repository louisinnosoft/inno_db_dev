return Aras.ES.ServerMethods.ClearMainCollection(this);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_ClearMainCollection' and [Method].is_current='1'">
<config_id>0A97DF2EB39740C0B4487912FF3317C4</config_id>
<name>ES_ClearMainCollection</name>
<comments>Clear main Solr collection</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
