/*
目的:產生PART的預設語系品名與幣別金額
做法:
1.將 Part 的 in_min_price in_unit_price merge 成 一個 In_part_Currency
2.將 Part 的 name merge 成 一個 In_part_languages
*/

Innovator inn = this.getInnovator();
string aml = "";

//1.將 Part 的 in_min_price in_unit_price merge 成 一個 In_part_Currency
//取得台幣的 Currency ID
aml = "<AML>";
aml += "<Item type='In_Currency' action='get'>";
aml += "<in_code>NT$</in_code>";
aml += "</Item></AML>";
Item itmCurrency = inn.applyAML(aml);

aml = "<AML>";
aml += "<Item type='In_part_Currency' action='merge' where=\"in_currency='" + itmCurrency.getID() + "' and [In_part_Currency].source_id='" + this.getID() + "'\">";
aml += "<in_min_price>" + this.getProperty("in_min_price","0") + "</in_min_price>";
aml += "<in_unit_price>" + this.getProperty("in_unit_price","0") + "</in_unit_price>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "<in_currency>" + itmCurrency.getID() + "</in_currency>";
aml += "</Item></AML>";
Item itmPartCurrency = inn.applyAML(aml);

//2.將 Part 的 name merge 成 一個 In_part_languages
aml = "<AML>";
aml += "<Item type='In_part_languages' action='merge' where=\"in_languages='zt' and [In_part_languages].source_id='" + this.getID() + "'\">";
aml += "<name>" + this.getProperty("name","") + "</name>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "<in_languages>zt</in_languages>";
aml += "</Item></AML>";
Item itmPartLanguage = inn.applyAML(aml);

return this;





#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GeneratePartCurrencyLanguage' and [Method].is_current='1'">
<config_id>C116AEF6779E4C95BCD0EB12BCDCFC63</config_id>
<name>In_GeneratePartCurrencyLanguage</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
