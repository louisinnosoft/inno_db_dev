/*
取得owned_by_id為當下登入使用者的會議並回傳。主要由c.aspx使用。

回傳物件結構：
    <Item>
        <!--本體啥都沒有-->
        <Relationships>
            <Item type="In_Meeting"/>
            <Item type="Inn_Param"/> <!--傳入的parameter，來自In_Collect_PassengerParam-->
        </Relationships>
    </Item>
*/
//System.Diagnostics.Debugger.Break();

Innovator inn=this.getInnovator();
Item itmRst=inn.newItem();//用來乘載回傳資料的物件。
Item itmCheckWithinTime=newItem("Method","In_Check_MeetingFunctionValid");
itmCheckWithinTime.setProperty("actiontype","2");//基於頁面需求，強制指定為簽到退(2)

Item itmParams=this.apply("In_Collect_PassengerParam");

string strUserName=inn.getItemById("User",inn.getUserID()).getProperty("keyed_name");
Item itmMeetings=inn.applyAML(@"
        <AML>
        	<Item type=""In_Meeting""  action=""get"">
        	<or>
        		<owned_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</owned_by_id>
        		<managed_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</managed_by_id>
            </or>
        	</Item>
        </AML>"
        .Replace("{#username}",strUserName));

int mtCount=itmMeetings.getItemCount();
for(int count=0;count<mtCount;count++){
    Item tmpMeeting=itmMeetings.getItemByIndex(count);
    itmCheckWithinTime.setProperty("meeting_id",tmpMeeting.getID());
    bool withinTime=itmCheckWithinTime.apply().getResult()=="1"; //0 : 不在時間內, 1:在時間內
    if(withinTime){
        itmRst.addRelationship(tmpMeeting);
    }
}

int paramCount=itmParams.getItemCount();
for(int pc=0;pc<paramCount;pc++){
    itmRst.addRelationship(itmParams.getItemByIndex(pc));
}

return itmRst;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingSignIn' and [Method].is_current='1'">
<config_id>97458406FD02466E90305627267FA70C</config_id>
<name>In_Get_MeetingSignIn</name>
<comments>取得會議簽到頁面所需要的資料。由c.aspx使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
