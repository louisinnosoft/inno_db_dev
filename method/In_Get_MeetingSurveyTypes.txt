//parameter : <meeting_id>請求的會議的id</meeting_id>
//filter : <filter>要過濾掉哪些種類的問題，如會議中不顯示〝事前問卷〞，格式為csv</filter>
//因為是在系統內部呼叫，所以不處理未傳送parameter的狀況。

Innovator inn=this.getInnovator();
string meeting_id=this.getProperty("meeting_id","CB12D9C4F9434AA7A7BC74A2F499855B");
string list_id="8C84BC25A86B4CF9AA62346BD7D06004";//in_surveytype的資料來源名稱
string filter_type=this.getProperty("filter","1111");//要過濾掉哪個種類的value，格式為csv
string[] qFilter=filter_type.Split(',');
Item result=inn.newItem();
Item typeList=inn.newItem();
typeList.setAttribute("type","List");
typeList.setAttribute("action","get");
typeList.setID(list_id);
typeList=typeList.apply();
typeList.fetchRelationships("Value");


Item allTypes=typeList.getRelationships("Value");

//取得所有種類的問卷
int atCount=allTypes.getItemCount();
for(int s=0;s<atCount;s++){
    bool isFiltered=false;
    Item surveyType=allTypes.getItemByIndex(s);
    Item sheets=inn.newItem();
    for(int x =0;x<qFilter.Length;x++){
        if(surveyType.getProperty("value")==qFilter[x]){continue;}
    }
    
    
    sheets.setAttribute("type","In_Meeting_Surveys");
    sheets.setAttribute("action","get");
    sheets.setAttribute("where","[In_Meeting_Surveys].in_surveytype='"+surveyType.getProperty("value")+"'");
    sheets.setProperty("source_id",meeting_id);
    sheets=sheets.apply();
    
    
    //數量不為0且不是被過濾掉的內容才傳回前端。
    if(!sheets.isEmpty()){
        result.addRelationship(surveyType);
    }
}

return result;
//return inn.newResult(result.dom.InnerXml);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingSurveyTypes' and [Method].is_current='1'">
<config_id>C1B2B840C28B45C79337C48220E2536C</config_id>
<name>In_Get_MeetingSurveyTypes</name>
<comments>取得一場會議有哪些種類的問卷，在系統內部呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
