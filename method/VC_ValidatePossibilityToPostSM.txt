Item item = this.newItem();
item.loadAML(string.Format("<Item type='{0}' id='{1}' action='VC_IsSSVCEnabledForItem' />", this.getProperty("item_type_name"), this.getProperty("item_id")));
item = item.apply();
if (item.isError())
{
	return this.getInnovator().newError(item.getErrorString());
}

return item.getResult() == "true"
	? this
	: this.getInnovator().newError(CCO.ErrorLookup.Lookup("SSVC_ItemNotEnabledToSSVC"));
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_ValidatePossibilityToPostSM' and [Method].is_current='1'">
<config_id>6D5EBE0F8F5740F5A18A516BEE8DABEE</config_id>
<name>VC_ValidatePossibilityToPostSM</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
