/*
目的:累計相同專案的所有客戶合約金額到專案
作法:
1.取得相同對象與專案的合約
2.取得專案的ID
3.取得合約的金額
4.加總合約的金額
5.將合約總金額更新至專案的金額
*/

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string err = "";

string strProjID = "";

double in_allow_amount = 0;
double in_allow_amount_ma = 0;

try
{
    string strFromState = this.getPropertyItem("transition").getPropertyAttribute("from_state","keyed_name");
    string strToState = this.getPropertyItem("transition").getPropertyAttribute("to_state","keyed_name");
    string strFromTo = strFromState + "_to_" + strToState;
    
    switch(strToState){
        
        case "Active":
        
			switch(this.getType()){
				
				case "in_contract":
				
					if(this.getProperty("classification","")=="客戶")
					{	
						//1.取得相同對象與專案的合約
						aml = "<AML>";
						aml += "<Item type='in_contract' action='get'>";
						aml += "<classification>"+this.getProperty("classification","")+"</classification>";
						aml += "<in_project>"+this.getProperty("in_project","")+"</in_project>";
						aml += "<state>Active</state>";
						aml += "</Item></AML>";
						Item itmContracts = inn.applyAML(aml);
						
						//2.取得專案的ID
						strProjID = this.getProperty("in_project","");
						
						for(int i=0;i<itmContracts.getItemCount();i++){
							Item itmContract = itmContracts.getItemByIndex(i);
							
							//3.取得合約的金額
							double dblSingle = Convert.ToDouble(itmContract.getProperty("in_allow_amount","0"));
							double dblSingle_ma = Convert.ToDouble(itmContract.getProperty("in_allow_amount_ma","0"));
							
							//4.加總合約的金額
							in_allow_amount += dblSingle;
							in_allow_amount_ma += dblSingle_ma;
						}
						
						//5.將合約總金額更新至專案的金額
						sql = "Update Project set ";
						sql += "in_contract_amount = " + in_allow_amount.ToString();
						sql += ",in_contract_amount_ma = " + in_allow_amount_ma.ToString();
						sql += " where id = '" + strProjID + "'";						
						inn.applySQL(sql);
						
						sql = "Update In_Budget set ";
						sql += "in_contract_amount = " + in_allow_amount.ToString();
						sql += ",in_contract_amount_ma = " + in_allow_amount_ma.ToString();
						sql += " where in_refproject = '" + strProjID + "' and in_year='" + System.DateTime.Now.ToString("yyyy") + "'";						
						inn.applySQL(sql);

						
					}
					
				break;
			}break;
    }
    
    
    
    if(err!="")
    throw new Exception(err);
}
catch(Exception ex){
    
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
    throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_BudgetPostPromote' and [Method].is_current='1'">
<config_id>27EA5E183A7F4E13944AC780A782AFBE</config_id>
<name>In_BudgetPostPromote</name>
<comments>累計相同專案的所有業主合約金額到專案</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
