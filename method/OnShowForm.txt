return ModulesManager.using(
	['aras.innovator.core.ItemWindow/FormItemWindowView',
	'aras.innovator.core.ItemWindow/DefaultItemWindowCreator']).then(function(View, Creator) {
		var view = new View(inDom,inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnShowForm' and [Method].is_current='1'">
<config_id>DA93E59F8A054BBB84EC70A571AD8288</config_id>
<name>OnShowForm</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
