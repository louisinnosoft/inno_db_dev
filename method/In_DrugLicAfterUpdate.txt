//in_DrugApprovalAfterUpdate
//藥證儲存後的相關動作
//1.提醒通知日期=有效日-提醒周期

Innovator inn = this.getInnovator();
string sql="";
int out_in_remindcycle;
Item tmp;
string in_remindcycle = this.getProperty("in_remindcycle","");

in_remindcycle = in_remindcycle.Replace("m","");
if(int.TryParse(in_remindcycle, out out_in_remindcycle))
{
	sql = "Update innovator.In_Drug_Lic set in_reminderdate = DATEADD(MONTH,-" + in_remindcycle + ",in_expirationdate)";
	sql += " Where id='" + this.getID() + "'";
	tmp = inn.applySQL(sql);
	
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DrugLicAfterUpdate' and [Method].is_current='1'">
<config_id>97E89227DFBB4C52A535C386897B9DE9</config_id>
<name>In_DrugLicAfterUpdate</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
