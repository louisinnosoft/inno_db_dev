//System.Diagnostics.Debugger.Break();
/*
目的:依據in_ref_itemtype_list找到正確的property id,填入in_ref_property
*/

Innovator inn = this.getInnovator();
string sql = "";
Item itmSQL;
if(this.getProperty("in_ref_itemtype_list","")=="")
{
	sql = "Update In_Sequence_Setting_Rules set in_ref_property = '' where id='" + this.getID() + "'";
	itmSQL = inn.applySQL(sql);
	return this;
}


	
string aml = "";
aml = "<AML>";
aml += "<Item type='In_Sequence_Setting' action='get' select='in_criteria(in_itemtype)' id='" + this.getProperty("source_id") + "'>";
aml += "</Item></AML>";

Item itmSeq = inn.applyAML(aml);

aml = "<AML>";
aml += "<Item type='Property' action='get'>";
aml += "<source_id>" + itmSeq.getPropertyItem("in_criteria").getProperty("in_itemtype") + "</source_id>";
aml += "<name>" + this.getProperty("in_ref_itemtype_list","") + "</name>";
aml += "</Item></AML>";

Item itmProp = inn.applyAML(aml);
if(itmProp.isError())
	throw new Exception("本物件[" +  itmSeq.getPropertyItem("in_criteria").getPropertyAttribute("in_itemtype","keyed_name") + "]不包含[" + this.getProperty("in_ref_itemtype_list","") + "]屬性");
	

sql = "Update In_Sequence_Setting_Rules set in_ref_property = '" + itmProp.getID() + "' where id='" + this.getID() + "'";

itmSQL = inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetSeqRuleProp' and [Method].is_current='1'">
<config_id>A0F33EFF28E940A3B80F1C71AAF6B04E</config_id>
<name>In_SetSeqRuleProp</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
