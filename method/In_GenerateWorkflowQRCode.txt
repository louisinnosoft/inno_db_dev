/*
目的:產生QRCode,並將圖片上傳到固定Property In_QRCODE
位置:OnAfterAdd/update
做法:
1.使用 InnUtility.GenerateQRCode 產生 QR Code JPG,並另存成file
2.將QR Code檔案上傳至Vault中
3.SQL Update
*/	
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
//1.產生QR Code,並另存成file
string strQRCodePath = CCO.Server.MapPath("../server/temp/" + this.getID() + ".jpg");
Item itmAppUrl = inn.getItemByKeyedName("In_Variable","app_url");
if(itmAppUrl==null)
	throw new Exception(_InnH.Translate("查無變數:[app_url],請檢查是否有設定app網址"));
string strAppUrl = itmAppUrl.getProperty("in_value","");
strAppUrl = strAppUrl + "?redir=Controls/ActVote.htm?itemtype=" + this.getType() + ":itemid=" + this.getID();

Innosoft.InnUtility.GenerateQRCode(strAppUrl ,strQRCodePath,300,300,1);		

//2.將QR Code檔案上傳至Vault中
Item itmQRCode = inn.newItem("File","add");
//itmQRCode.setProperty("filename","QRCode-" + this.getType() + "-" + this.getID() + ".jpg");

itmQRCode.setProperty("filename",this.getProperty("item_number","") + ".jpg");
itmQRCode.attachPhysicalFile(strQRCodePath);
itmQRCode = itmQRCode.apply();

//3.SQL Update
string sql  = "";
sql = "Update [" + this.getType().Replace(" ","_") + "] set in_qrcode='" + itmQRCode.getID() + "'";
sql += " where id='" + this.getID() + "'";
Item itmSQL = inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GenerateWorkflowQRCode' and [Method].is_current='1'">
<config_id>F2BC85492B4549AD928B76B871819037</config_id>
<name>In_GenerateWorkflowQRCode</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
