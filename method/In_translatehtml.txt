/*
此方法會去抓取translate這個資料夾下包括子目錄的所有檔案並將內容做繁轉簡，抓取的檔案類型為html、htm、aspx、cs、js
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string Root_Path = @"C:\Program Files (x86)\Aras\plm\Innovator\Server\temp\translate";
string InputText = "";
string []FilesPath = Directory.GetFiles(Root_Path, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".html") || s.EndsWith(".htm") || s.EndsWith(".aspx") || s.EndsWith(".cs") || s.EndsWith(".js")).ToArray();

for (int i = 0; i < FilesPath.Length; i++)
{
    InputText = File.ReadAllText(FilesPath[i], Encoding.UTF8);//讀入字串
    InputText = Innosoft.InnUtility.ConvertLanguage(InputText, "zc");//轉成簡體
    File.WriteAllText(FilesPath[i], InputText, Encoding.UTF8);//輸出檔案
}
return inn.newResult("受影響的檔案:\n"+String.Join("\n", FilesPath).Replace(Root_Path,""));
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_translatehtml' and [Method].is_current='1'">
<config_id>A8209F3F0B74472FAC5B102C86B384CE</config_id>
<name>In_translatehtml</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
