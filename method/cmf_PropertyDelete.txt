Item itemTypeQuery = this.newItem("cmf_PropertyType", "get");
itemTypeQuery.setID(this.getID());
itemTypeQuery.setAttribute("select", "generated_type, source_id");
itemTypeQuery = itemTypeQuery.apply();
string itemTypeId = itemTypeQuery.getProperty("generated_type");
string documentElementTypeId = itemTypeQuery.getProperty("source_id");

if (string.IsNullOrEmpty(itemTypeId)) return this;

Item documentTypeQuery = this.newItem("cmf_ElementType", "get");
documentTypeQuery.setID(documentElementTypeId);
documentTypeQuery.setAttribute("select", "source_id");
documentTypeQuery = documentTypeQuery.apply();
string documentTypeId = documentTypeQuery.getProperty("source_id");

if (RequestState.Contains(documentTypeId))
{
    ((List<string>) RequestState[documentTypeId]).Add(itemTypeId);
    return this;
}
else
{
    return this.getInnovator().newError("It's impossible to save certain Element Type or Property Type. Instead save entire Content Type item.");
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_PropertyDelete' and [Method].is_current='1'">
<config_id>9BD70A454530444FAEDA44D4D27265AB</config_id>
<name>cmf_PropertyDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
