

string fetch =this.getProperty("fetch","0");
if(fetch!="4" && fetch!="5"){
	return this;
}


var aliasId = this.getInnovator().getUserAliases();
var alias = this.getInnovator().getItemById("Identity",aliasId);

this.setProperty("in_pm",alias.getProperty("id"));

return this;
				
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetD_proposalPm' and [Method].is_current='1'">
<config_id>79525CF4EC374964958EFA8E830D80A9</config_id>
<name>In_SetD_proposalPm</name>
<comments>新增時設定in_pm為當下使用者</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
