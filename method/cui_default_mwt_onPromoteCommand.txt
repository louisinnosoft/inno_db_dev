var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onPromoteCommand) {
	workerFrame.onPromoteCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onPromoteCommand' and [Method].is_current='1'">
<config_id>06E75D0E4CBD44179FAA404B952EB17F</config_id>
<name>cui_default_mwt_onPromoteCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
