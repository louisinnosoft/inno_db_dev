/*
目的：將預算子項目的 會計科目物件的 編號與分類 謄寫到in_second_subjects與in_subjects
必須同時適用related item 為 Part與非 Part 的Item
 */

Innovator inn = this.getInnovator();
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
try
{
	
	if(this.getProperty("related_id","")!="")
	{
		string aml = "<AML>";
		aml += "<Item type='Part' action='get' select='in_accounting'>";
		aml += "<id>" + this.getProperty("related_id","") + "</id>";
		aml += "</Item></AML>";		
		Item itmPart = inn.applyAML(aml);
		
		string sql = "Update [" + this.getType().Replace(" ","_") + "] set ";
		sql += "in_accounting='" + itmPart.getProperty("in_accounting","") + "'";
		sql += " where id='" + this.getID() + "'";
		inn.applySQL(sql);
		
		this.setProperty("in_accounting",itmPart.getProperty("in_accounting",""));		
		this.setPropertyAttribute("in_accounting","type","In_Accounting_items");
	}
	
	//(from,to;from,to;from,to)
	string CopyProperties="in_accounting_number,in_second_subjects;in_accounting_subjects,in_subjects"; //描述方式為兩兩一組
	
	Item itmResult = _InnH.UpdatePropertyFromPropertyItem(this,"in_accounting",CopyProperties,"1"); //0:若欄位已有值,則不覆蓋,1:直接覆蓋,2:累加內容
	
}
catch (Exception ex)
{
	throw new Exception(_InnH.Translate(ex.Message));
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateAccountingNumber' and [Method].is_current='1'">
<config_id>19F66BAAD4CC42D88EE63E455B468B58</config_id>
<name>In_UpdateAccountingNumber</name>
<comments>將預算子項目的 會計科目物件的 編號與分類 謄寫到in_second_subjects與in_subjects</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
