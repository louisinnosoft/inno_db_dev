aras.getMostTopWindowWithAras(window)['system_ItemTypeHideTabs'] = function() {
	// Used as onload event for ItemType Form.
	// Make sure there is a relationships frame
	if (!parent.relationships) {
		setTimeout(function() {
			aras.getMostTopWindowWithAras(window)['system_ItemTypeHideTabs']();
		}, 100);
		return;
	}

	var tabsToHide = {
		'polymorphic': ['Allowed Permission', 'Allowed Workflow', 'Client Event', 'Item Action', 'Item Report',
			'RelationshipType', 'View', 'DiscussionTemplate'],
		'federated': ['DiscussionTemplate', 'Morphae'],
		'table': ['Morphae'] //What is table? In UI it's called "Single Item"
	};

	function getNameToIdDictionary(tabsObj) {
		var dictionary = [];
		var keys = Object.keys(tabsObj);
		keys.forEach(function(key) {
			var tabs = tabsObj[key];
			tabs.forEach(function(el) {
				if (!dictionary[el]) {
					dictionary[el] = aras.getRelationshipTypeId(el);
				}
			});
		});

		return dictionary;
	}

	if (parent.relationships.document.readyState != 'complete' || !parent.relationships.currTabID) {
		setTimeout(function() {
			aras.getMostTopWindowWithAras(window)['system_ItemTypeHideTabs']();
		}, 100);
	} else {
		var implementationType = parent.item.selectSingleNode('implementation_type');
		var implementationTypeText = '';
		if (implementationType) {
			implementationTypeText = implementationType.text;
		}

		var relationshipTypesDictionary = getNameToIdDictionary(tabsToHide);

		parent.relationships.relTabbar.getTabOrder().split(',').forEach(function(tabId) {
			parent.relationships.relTabbar.setTabEnabled(tabId, 1);
		});

		tabsToHide[implementationTypeText].forEach(function(tabName) {
			parent.relationships.relTabbar.setTabEnabled(relationshipTypesDictionary[tabName], 0);
		});

		var tabIds = parent.relationships.relTabbar.getTabOrder('|');
		var a = tabIds.split('|');
		// Show the first tab
		parent.relationships.relTabbar.selectTab(a[0]);
	}
};

// Wait a bit before starting
setTimeout(function() {
	aras.getMostTopWindowWithAras(window)['system_ItemTypeHideTabs']();
}, 200);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ItemType Form onload' and [Method].is_current='1'">
<config_id>714FB2F57E6B4036857EBEBA5FDD4E23</config_id>
<name>ItemType Form onload</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
