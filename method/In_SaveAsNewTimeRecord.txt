/*
目的:另存工時卡
*/
var inn = this.getInnovator();
var itmOldTimeRec = inn.getItemById("In_TimeRecord",this.getProperty("related_id"));

var itmTimeRec = this.newItem("In_TimeRecord","add");
itmTimeRec.setProperty("in_function_code",itmOldTimeRec.getProperty("in_function_code",""));
itmTimeRec.setPropertyAttribute("in_function_code","keyed_name",itmOldTimeRec.getPropertyAttribute("in_function_code","keyed_name"));
itmTimeRec.setProperty("in_project",itmOldTimeRec.getProperty("in_project",""));
itmTimeRec.setPropertyAttribute("in_project","keyed_name",itmOldTimeRec.getPropertyAttribute("in_project","keyed_name"));

itmTimeRec.setProperty("owned_by_id",itmOldTimeRec.getProperty("owned_by_id",""));
itmTimeRec.setPropertyAttribute("owned_by_id","keyed_name",itmOldTimeRec.getPropertyAttribute("owned_by_id","keyed_name"));


itmTimeRec.setProperty("in_notes",itmOldTimeRec.getProperty("in_notes",""));
itmTimeRec.setProperty("in_work_hours",itmOldTimeRec.getProperty("in_work_hours",""));
itmTimeRec.setProperty("in_start_time",itmOldTimeRec.getProperty("in_start_time",""));

var itmPerlTimeRec = this.newItem("In_PerlWorkRecord_time","add");
itmPerlTimeRec.setPropertyItem("related_id",itmTimeRec);

parent.thisItem.addRelationship(itmPerlTimeRec);
return window.onRefresh();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SaveAsNewTimeRecord' and [Method].is_current='1'">
<config_id>BC24BEB6A99F419B9D83C742E81FE8CC</config_id>
<name>In_SaveAsNewTimeRecord</name>
<comments>另存工時卡</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
